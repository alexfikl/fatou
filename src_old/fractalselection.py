# SPDX-FileCopyrightText: 2011,2022 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt5.QtCore import QDir, Qt, pyqtSignal
from PyQt5.QtWidgets import (QDialog, QFileDialog, QHBoxLayout, QMessageBox,
                             QPushButton, QTreeWidget, QTreeWidgetItem,
                             QVBoxLayout)


class FractalSelectionDialog(QDialog):
    loadPlugin = pyqtSignal(str)
    installPlugin = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)

        self._pluginIds = []
        self._errorMessages = {
            1: "Not a valid zip file.",
            2: "Plugin does not contain necessary files.",
            3: "Plugin does not contain necessary classes.",
            4: "A plugin with this name is already installed.",
        }

        self._createLayout()
        self.setWindowTitle("Fractal Selection")
        self.resize(300, 500)

    def setPluginList(self, plist, error):
        if error != 0:
            QMessageBox.critical(self, "Error", self._errorMessages[error])
        else:
            self._populateListWidget(plist)

    def _createLayout(self):
        self._installPluginButton = QPushButton("Install Plugin")
        self._installPluginButton.clicked.connect(self._openFileDialog)

        self._selectPluginButton = QPushButton("Select Plugin")
        self._selectPluginButton.setEnabled(False)
        self._selectPluginButton.clicked.connect(self._itemSelected)
        self._selectPluginButton.clicked.connect(self.close)

        self._cancelButton = QPushButton("Cancel")
        self._cancelButton.clicked.connect(self.close)

        self._pluginListWidget = QTreeWidget(self)
        self._pluginListWidget.setColumnCount(1)
        self._pluginListWidget.headerItem().setHidden(True)
        self._pluginListWidget.setMaximumWidth(350)
        self._pluginListWidget.itemClicked.connect(self._enableSelect)

        mainlayout = QVBoxLayout(self)
        mainlayout.addWidget(self._pluginListWidget)

        layout = QHBoxLayout()
        layout.addStretch()
        layout.addWidget(self._installPluginButton, 0, Qt.AlignCenter)
        layout.addWidget(self._selectPluginButton, 0, Qt.AlignCenter)
        layout.addWidget(self._cancelButton, 0, Qt.AlignCenter)

        mainlayout.addLayout(layout)

    def _populateListWidget(self, plist):
        self._pluginListWidget.clear()
        for plugin_id, plugin_name, plugin_fractals in plist:
            self._pluginIds.append(plugin_id)
            category_item = QTreeWidgetItem(self._pluginListWidget)
            category_item.setText(0, plugin_name)

            if not plugin_fractals:
                continue

            for fractal in plugin_fractals:
                litem = QTreeWidgetItem(category_item)
                litem.setText(0, fractal)

        self._pluginListWidget.expandAll()

    def _itemSelected(self):
        plugin_id = self._pluginListWidget.currentItem()
        plugin_id = self._pluginListWidget.indexOfTopLevelItem(plugin_id)
        self.loadPlugin.emit(str(self._pluginIds[plugin_id]))

    def _enableSelect(self, item, _):
        self._selectPluginButton.setEnabled(True if not item.parent() else False)

    def _openFileDialog(self):
        filename = QFileDialog.getOpenFileName(
            self, "Install Plugin", QDir.homePath(), "Zip File (*.zip)"
        )

        if not filename.isNull():
            self.installPlugin.emit(str(filename))


if __name__ == "__main__":
    import sys

    from PyQt5.QtWidgets import QApplication

    from pluginmanager import PluginManager

    app = QApplication(sys.argv)
    pm = PluginManager()
    dialog = FractalSelectionDialog()
    dialog.setPluginList(pm.listPlugins(), 0)
    dialog.show()

    sys.exit(app.exec_())
