# SPDX-FileCopyrightText: 2011,2022 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt5.QtCore import QSettings, Qt, pyqtSignal
from PyQt5.QtWidgets import (QComboBox, QFormLayout, QGroupBox, QHBoxLayout,
                             QLineEdit, QMessageBox, QPushButton, QSpinBox,
                             QTableWidget, QTableWidgetItem, QVBoxLayout)
from PyQt5.QtWidgets import qApp

from plugins.abstractfractal import AbstractFractalOptions
from plugins.lsystems.info import FRACTAL_PRESET_OPTIONS, FRACTALS


class RulesEdit(QTableWidget):
    rulesChanged = pyqtSignal(dict)

    def __init__(self, rules, parent=None):
        QTableWidget.__init__(self, parent)
        self._settingRules = False

        self.verticalHeader().hide()
        self.setColumnCount(2)
        self.setRowCount(len(rules))
        self.setColumnWidth(0, 40)
        self.setHorizontalHeaderLabels(["From", "To"])
        self.setRules(rules)

        self.cellChanged.connect(self.__toUpper)

    def __toUpper(self, row, column):
        item = self.item(row, column)
        text = []
        for ch in str(item.data(Qt.DisplayRole).toString()):
            if ch != "f":
                text.append(ch.upper())
            else:
                text.append(ch)
        text = "".join(text)
        item.setData(Qt.DisplayRole, text)

        if not column:
            item.setTextAlignment(Qt.AlignCenter)

        if not self._settingRules:
            self.rulesChanged.emit(self.getRules())

    def addRow(self):
        row = self.rowCount()
        self.insertRow(row)

    def getRules(self):
        rules = {}
        for i in range(self.rowCount()):
            first = str(self.item(i, 0).data(Qt.DisplayRole).toString())
            last = str(self.item(i, 1).data(Qt.DisplayRole).toString())
            if first and last:
                rules[first] = last
        return rules

    def setRules(self, rules):
        self._settingRules = True
        self.clearRules()
        self.setRowCount(len(rules))
        i = 0
        for key, value in list(rules.items()):
            item = QTableWidgetItem(key)
            item.setTextAlignment(Qt.AlignCenter)
            self.setItem(i, 0, item)

            item = QTableWidgetItem(value)
            self.setItem(i, 1, item)
            i += 1
        self._settingRules = False

    def resizeEvent(self, ev):
        col_size = self.size().width() - 45
        self.setColumnWidth(1, col_size)

    def isEmpty(self):
        for i in range(self.rowCount()):
            try:
                first = str(self.item(i, 0).data(Qt.DisplayRole).toString())
                last = str(self.item(i, 1).data(Qt.DisplayRole).toString())
                if last and first:
                    return False
            except AttributeError as e:
                print("error", e)

        return True

    def clearRules(self):
        self.clear()
        self.setRowCount(1)
        self.setHorizontalHeaderLabels(["From", "To"])


class LindenmayerSystemsOptions(AbstractFractalOptions):
    saveToFile = pyqtSignal(str)
    iterationsChanged = pyqtSignal(int)
    axiomChanged = pyqtSignal(str)
    angleChanged = pyqtSignal(int)
    rulesChanged = pyqtSignal(dict)
    rotationChanged = pyqtSignal(int)
    start = pyqtSignal()
    stop = pyqtSignal()

    def __init__(self, parent=None):
        AbstractFractalOptions.__init__(self, parent)

        self.__readSettings()
        self.__createWidgets()
        self.__createLayout()

        qApp.aboutToQuit.connect(self.saveSettings)
        self.setMinimumWidth(160)
        self.setMaximumWidth(220)

    def __readSettings(self):
        settings = QSettings("fatou", "fatou")

        settings.beginGroup("lsystems")
        self._current = settings.value("current", 5).toInt()[0]
        self._angleV = settings.value("angle", 45).toInt()[0]
        self._iterationsV = settings.value("iterations", 10).toInt()[0]
        self._rotation = settings.value("rotation", 0).toInt()[0]
        self._axiomV = settings.value("axiom", "L--F--L--F").toString()
        self._rulesV = str(settings.value("rules", "L:+R-F-R+;;R:-L+F+L-").toString())
        settings.endGroup()

        self._rulesV = dict([x.split(":") for x in self._rulesV.split(";;")])

    def saveSettings(self):
        settings = QSettings("fatou", "fatou")

        rules = self._rules.getRules()
        rules = ";;".join([":".join(x) for x in list(rules.items())])

        settings.beginGroup("lsystems")
        settings.setValue("current", self._presets.currentIndex())
        settings.setValue("angle", self._angle.value())
        settings.setValue("iterations", self._iterations.value())
        settings.setValue("rotation", self._rotation)
        settings.setValue("axiom", self._axiom.text())
        settings.setValue("rules", rules)
        settings.endGroup()

    def __createWidgets(self):
        self._presets = QComboBox(self)
        for f in FRACTALS:
            self._presets.addItem(f)
        self._presets.activated.connect(self.__setPresetValues)
        self._presets.setCurrentIndex(self._current)

        self._iterations = QSpinBox(self)
        self._iterations.setRange(1, 100)
        self._iterations.setValue(self._iterationsV)
        self._iterations.valueChanged.connect(self.iterationsChanged)

        self._axiom = QLineEdit(self)
        self._axiom.setText(self._axiomV)
        self._axiom.textChanged.connect(self.axiomChanged)

        self._angle = QSpinBox(self)
        self._angle.setRange(0, 360)
        self._angle.setValue(self._angleV)
        self._angle.valueChanged.connect(self.angleChanged)

        self._rules = RulesEdit(self._rulesV, self)
        self._rules.rulesChanged.connect(self.rulesChanged)

        self._addRow = QPushButton("Add Rule")
        self._addRow.clicked.connect(self._rules.addRow)

        self._clearRules = QPushButton("Clear Rules")
        self._addRow.clicked.connect(self._rules.clearRules)

    def __createLayout(self):
        main_layout = QVBoxLayout(self)

        layout = QFormLayout()
        layout.addRow("Iterations:", self._iterations)
        layout.addRow("Axiom:", self._axiom)
        layout.addRow("Angle:", self._angle)

        group = QGroupBox("Rules")
        l1 = QVBoxLayout()
        l1.addWidget(self._rules)
        l2 = QHBoxLayout()
        l2.addWidget(self._addRow)
        l2.addWidget(self._clearRules)
        l1.addLayout(l2)
        group.setLayout(l1)

        main_layout.addWidget(self._presets)
        main_layout.addLayout(layout)
        main_layout.addWidget(group)
        main_layout.addStretch()

        self.setLayout(main_layout)

    def __checker(self):
        if self._axiom.text().isEmpty():
            QMessageBox.warning(
                self, "Warning", "There is no starting variable defined for the system."
            )
        elif self._rules.isEmpty():
            QMessageBox.warning(
                self, "Warning", "There are no rules defined for the system."
            )
        elif not any(
            [f in self._axiom.text() for f, t in list(self._rules.getRules().items())]
        ):
            QMessageBox.warning(
                self,
                "Warning",
                "Starting variable cannot be found in any defined rule.",
            )
        else:
            self.emitStartData()

    def emitStartData(self):
        self.angleChanged.emit(self._angle.value())
        self.iterationsChanged.emit(self._iterations.value())
        self.axiomChanged.emit(self._axiom.text())
        self.rulesChanged.emit(self._rules.getRules())
        self.rotationChanged.emit(self._rotation)
        self.start.emit()

    def __setPresetValues(self, index):
        current = FRACTAL_PRESET_OPTIONS[index]
        self._angle.setValue(current["angle"])
        self._iterations.setValue(current["its"])
        self._axiom.setText(current["axiom"])
        self._rules.setRules(current["rules"])
        self._rotation = current["rotate"]


if __name__ == "__main__":
    import sys

    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    ops = LindenmayerSystemsOptions()
    qApp.aboutToQuit.connect(ops.saveSettings)
    ops.show()

    sys.exit(app.exec_())
