# SPDX-FileCopyrightText: 2011,2022 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

NAME = "Lindenmayer Systems"
FRACTALS = [
    "Levy Dragon",
    "Koch Snowflake",
    "Levy C",
    "Koch Curve",
    "Sierpinski Triangle",
    "Sierpinski Median",
    "Peano",
    "Square Sierpinski",
    "Islands and Lakes",
    "Koch Cubes",
    "Koch Moss",
    "Plant 1",
    "Plant 2",
    "Plant 3",
]

FRACTAL_PRESET_OPTIONS = [
    {
        "angle": 90,
        "its": 10,
        "axiom": "FX",
        "rules": {"X": "X+YF+", "Y": "-FX-Y"},
        "rotate": 0,
    },
    {
        "angle": 60,
        "its": 4,
        "axiom": "F++F++F",
        "rules": {"F": "F-F++F-F"},
        "rotate": 0,
    },
    {"angle": 45, "its": 10, "axiom": "F", "rules": {"F": "+F--F+"}, "rotate": -45},
    {
        "angle": 90,
        "its": 3,
        "axiom": "F-F-F-F",
        "rules": {"F": "FF-F-F-F-F-F+F"},
        "rotate": 0,
    },
    {
        "angle": 60,
        "its": 7,
        "axiom": "A",
        "rules": {"A": "B-A-B", "B": "A+B+A"},
        "rotate": 180,
    },
    {
        "angle": 45,
        "its": 10,
        "axiom": "L--F--L--F",
        "rules": {"L": "+R-F-R+", "R": "-L+F+L-"},
        "rotate": 45,
    },
    {
        "angle": 90,
        "its": 3,
        "axiom": "X",
        "rules": {"X": "XFYFX+F+YFXFY-F-XFYFX", "Y": "YFXFY-F-XFYFX+F+YFXFY"},
        "rotate": 0,
    },
    {
        "angle": 90,
        "its": 3,
        "axiom": "F+XF+F+XF",
        "rules": {"X": "XF-F+F-XF+F+XF-F+F-X"},
        "rotate": 0,
    },
    {
        "angle": 90,
        "its": 2,
        "axiom": "F+F+F+F",
        "rules": {"F": "F+f-FF+F+FF+Ff+FF-f+FF-F-FF-Ff-FFF", "f": "ffffff"},
        "rotate": 0,
    },
    {
        "angle": 90,
        "its": 3,
        "axiom": "F-F-F-F",
        "rules": {"F": "FF-F-F-F-FF"},
        "rotate": 0,
    },
    {
        "angle": 90,
        "its": 4,
        "axiom": "F-F-F-F",
        "rules": {"F": "FF-F--F-F"},
        "rotate": 0,
    },
    {
        "angle": 25,
        "its": 5,
        "axiom": "X",
        "rules": {"X": "F-[[X]+X]+F[+FX]-X", "F": "FF"},
        "rotate": 240,
    },
    {
        "angle": 7,
        "its": 6,
        "axiom": "+++++++++++++X",
        "rules": {"X": "F[@.5+++++++++!X]-F[@.4-----------X]@.6X"},
        "rotate": 180,
    },
    {
        "angle": 26,
        "its": 6,
        "axiom": "X",
        "rules": {"X": "F[+X][-X]FX", "F": "FF"},
        "rotate": -117,
    },
]
