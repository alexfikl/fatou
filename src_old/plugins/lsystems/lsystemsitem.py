# SPDX-FileCopyrightText: 2011,2022 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import math
import re

from PyQt5.QtCore import QRectF, Qt
from PyQt5.QtWidgets import QGraphicsItem
from PyQt5.QtGui import QPen


class LindenmayerSystemsItem(QGraphicsItem):
    def __init__(self, link):
        QGraphicsItem.__init__(self)

        self._link = link
        self._xsize = 300
        self._ysize = 300
        self._drawing = True
        self._angle = 0
        self._numbers = []
        self._rules = {}
        self._iterations = 0
        self._fractal = ""

        self._actions = {
            "F": self.__drawForward,
            "A": self.__drawForward,
            "B": self.__drawForward,
            "f": self.__moveForward,
            "+": self.__turnRight,
            "-": self.__turnLeft,
            "[": self.__storeState,
            "]": self.__restoreState,
            "@": self.__multiplyLength,
            "!": self.__invertTurns,
        }

        self.__initState()

    def __generate(self):
        state = self._axiom
        for _ in range(self._iterations):
            state2 = []
            for character in state:
                state2.append(self._rules.get(character, character))
            state = "".join(state2)
        return state

    def __adjustHeight(self):
        self._drawing = False
        self.__initState()

        for ch in self._fractal:
            self._actions.get(ch, lambda: None)()

        self._drawing = True
        if self._ysize <= (self._y["max"] - self._y["min"]):
            self._ysize = (
                self._ysize
                * (self._y["max"] - self._y["min"])
                / (self._x["max"] - self._x["min"])
            )
        else:
            self._xsize = (
                self._xsize
                * (self._x["max"] - self._x["min"])
                / (self._y["max"] - self._y["min"])
            )

    def __initState(self, init_all=True):
        self._turtleState = []
        if init_all:
            self._xsize = 300
            self._ysize = 300
            self._x = {}
            self._y = {}
            self._x["min"] = self._x["max"] = 0.0
            self._y["min"] = self._y["max"] = 0.0
        self._x["j"] = self._x["x"] = 0.0
        self._y["j"] = self._y["y"] = 0.0
        self._cAngle = self._angle
        self._cLength = 1
        self._cNumbers = self._numbers[:]

    def __next(self, val):
        # (x - xmin) / (xmax - xmin) * size
        if val == "x":
            return int(
                (self._x["x"] - self._x["min"])
                / (self._x["max"] - self._x["min"])
                * (self._xsize - 1)
            )
        else:
            return int(
                (self._y["y"] - self._y["min"])
                / (self._y["max"] - self._y["min"])
                * (self._ysize - 1)
            )

    def paint(self, painter, options, widget):
        if not self._fractal:
            print("nothing to draw")
            return

        self.__initState(False)
        self._painter = painter

        pen = QPen(Qt.white, 1, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        self._painter.setPen(pen)

        self._x["j"] = self.__next("x")
        self._y["j"] = self.__next("y")
        for ch in self._fractal:
            self._actions.get(ch, lambda: None)()

        self._link.stopped.emit()

    def __drawForward(self, draw=True):
        if self._drawing:
            x0 = self._x["x"] + self._cLength * math.cos(self._cAngle)
            y0 = self._y["y"] + self._cLength * math.sin(self._cAngle)

            jx0 = self.__next("x")
            jy0 = self.__next("y")

            if draw:
                self._painter.drawLine(self._x["j"], self._y["j"], jx0, jy0)

            self._x["x"] = x0
            self._y["y"] = y0
            self._x["j"] = jx0
            self._y["j"] = jy0
        else:
            # this is only used for adjusting the height
            # there should be a better way to do this
            self._x["x"] += self._cLength * math.cos(self._cAngle)
            self._y["y"] += self._cLength * math.sin(self._cAngle)

            self._x["min"] = min(self._x["x"], self._x["min"])
            self._x["max"] = max(self._x["x"], self._x["max"])
            self._y["min"] = min(self._y["y"], self._y["min"])
            self._y["max"] = max(self._y["y"], self._y["max"])

    def boundingRect(self):
        return QRectF(0, 0, 500, 500)

    def __moveForward(self):
        self.__drawForward(False)

    def __turnRight(self):
        self._cAngle += self._angle

    def __turnLeft(self):
        self._cAngle -= self._angle

    def __storeState(self):
        state = []
        state.append(self._x["x"])
        state.append(self._y["y"])
        state.append(self._cLength)
        state.append(self._cAngle)
        if self._drawing:
            state.append(self._x["j"])
            state.append(self._y["j"])

        self._turtleState.append(state)

    def __restoreState(self):
        state = self._turtleState.pop()
        self._x["x"] = state[0]
        self._y["y"] = state[1]
        self._cLength = state[2]
        self._cAngle = state[3]
        if self._drawing:
            self._x["j"] = state[4]
            self._y["j"] = state[5]

    def __multiplyLength(self):
        self._cLength *= float(self._cNumbers.pop(0))

    def __invertTurns(self):
        self._angle = -self._angle

    def setAxiom(self, axiom):
        self._axiom = str(axiom)

    def setRules(self, rules):
        self._rules = rules

    def setAngle(self, angle):
        self._angle = 2.0 * angle * math.pi / 360

    def setIterations(self, its):
        self._iterations = its

    def start(self):
        # generate lsystem for given iterations
        self._fractal = self.__generate()
        self._numbers = re.findall(r"(\d*\.?\d+)", self._fractal)
        self.__adjustHeight()
        self.update()
