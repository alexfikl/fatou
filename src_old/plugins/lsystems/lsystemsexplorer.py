# SPDX-FileCopyrightText: 2011,2022 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QHBoxLayout

from plugins.abstractfractal import AbstractFractalExplorer

from .lsystemsoptions import LindenmayerSystemsOptions
from .lsystemsview import LindenmayerSystemsView


class LsystemsExplorer(AbstractFractalExplorer):
    saveTo = pyqtSignal(str)
    start = pyqtSignal()
    stopped = pyqtSignal()
    zoomIn = pyqtSignal()
    zoomOut = pyqtSignal()

    iterationsChanged = pyqtSignal(int)
    axiomChanged = pyqtSignal(str)
    angleChanged = pyqtSignal(int)
    rulesChanged = pyqtSignal(dict)

    def __init__(self, parent=None):
        AbstractFractalExplorer.__init__(self, parent)
        self.__createWidgets()

        self.saveTo.connect(self._lsystemsView.saveToFile)
        self.start.connect(self._lsystemsOptions.emitStartData)
        self.zoomIn.connect(self._lsystemsView.zoomIn)
        self.zoomOut.connect(self._lsystemsView.zoomOut)

        self._lsystemsView.stopped.connect(self.stopped)

        self._lsystemsOptions.iterationsChanged.connect(
            self._lsystemsView.iterationsChanged
        )
        self._lsystemsOptions.angleChanged.connect(self._lsystemsView.angleChanged)
        self._lsystemsOptions.axiomChanged.connect(self._lsystemsView.axiomChanged)
        self._lsystemsOptions.rulesChanged.connect(self._lsystemsView.rulesChanged)
        self._lsystemsOptions.rotationChanged.connect(self._lsystemsView.setRotation)
        self._lsystemsOptions.start.connect(self._lsystemsView.start)

    def __createWidgets(self):
        self._lsystemsView = LindenmayerSystemsView(self)
        self._lsystemsOptions = LindenmayerSystemsOptions(self)

        layout = QHBoxLayout()
        layout.addWidget(self._lsystemsView)
        layout.addWidget(self._lsystemsOptions)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)


if __name__ == "__main__":
    import sys

    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    ex = LsystemsExplorer()
    ex.resize(700, 500)
    ex.show()

    sys.exit(app.exec_())
