#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


from PyQt5.QtCore import *
from PyQt5.QtGui import *

import plugins.abstractfractal as af
from gradientdialog import GradientDialog

from .info import FRACTALS


class MandelbrotOptions(af.AbstractFractalOptions):
    typeChanged = pyqtSignal(int)
    iterationsChanged = pyqtSignal(int)
    colorDensityChanged = pyqtSignal(int, bool)
    colormapSizeChanged = pyqtSignal(int, bool)
    gradientStopsChanged = pyqtSignal(list, bool)
    juliaXChanged = pyqtSignal(float)
    juliaYChanged = pyqtSignal(float)

    smoothColoringToggle = pyqtSignal(int, bool)
    juliaToggle = pyqtSignal(int, bool)
    start = pyqtSignal()

    def __init__(self, parent=None):
        af.AbstractFractalOptions.__init__(self, parent)

        self.__readSettings()
        self._gradientDialog = None

        self.__createWidgets()
        self.__createLayout()
        self.setFixedWidth(260)

    def __readSettings(self):
        settings = QSettings("fatou", "fatou")

        settings.beginGroup("mandelbrot")
        self._current = settings.value("current", 0).toInt()[0]
        self._iterationsV = settings.value("iterations", 50).toInt()[0]
        self._colord = settings.value("color-density", 1).toInt()[0]
        self._colors = settings.value("colormap-size", 512).toInt()[0]
        self._smooth = settings.value("smooth-coloring", False).toBool()
        self._juliaV = settings.value("julia", False).toBool()
        self._juliaX = settings.value("julia-x", "0.156").toString()
        self._juliaY = settings.value("julia-y", "0.8").toString()
        self._gradientStops = str(settings.value("gradient", "").toString())
        settings.endGroup()

        if not self._gradientStops:
            self._gradientStops = ["0.00,255,255,255"]
            self._gradientStops.append("0.10,224,204,115")
            self._gradientStops.append("0.17,198,169,6")
            self._gradientStops.append("0.46,96,6,89")
            self._gradientStops.append("0.72,6,128,172")
            self._gradientStops.append("0.92,185,217,262")
            self._gradientStops.append("1.00,255,255,255")
            self._gradientStops = ";;".join(self._gradientStops)

        gradient = []
        for i in self._gradientStops.split(";;"):
            stop = i.split(",")
            stop[0] = float(stop[0])
            color = QColor(int(stop[1]), int(stop[2]), int(stop[3]))
            gradient.append((stop[0], color))
        self._gradientStops = gradient

    def saveSettings(self):
        settings = QSettings("fatou", "fatou")

        gradient = []
        for p, color in self._gradientStops:
            r, g, b, a = color.getRgb()
            gradient.append([p, r, g, b])
        gradient = ";;".join([",".join([str(j) for j in i]) for i in gradient])

        smoothState = False if self._smoothColoring.checkState() == 0 else True
        juliaState = False if self._julia.checkState() == 0 else True

        settings.beginGroup("mandelbrot")
        settings.setValue("current", self._presets.currentIndex())
        settings.setValue("iterations", self._iterationsLimit.value())
        settings.setValue("color-density", self._colorDensity.value())
        settings.setValue("colormap-size", self._colormapSize.value())
        settings.setValue("smooth-coloring", smoothState)
        settings.setValue("julia", juliaState)
        settings.setValue("julia-x", self._startJuliaX.value())
        settings.setValue("julia-y", self._startJuliaY.value())
        settings.setValue("gradient", gradient)
        settings.endGroup()

    def __createWidgets(self):
        self._presets = QComboBox(self)
        for index, f in enumerate(FRACTALS):
            self._presets.addItem(f)
        self._presets.setCurrentIndex(self._current)

        self._startJuliaX = QDoubleSpinBox()
        self._startJuliaX.setRange(-4.0, 4.0)
        self._startJuliaX.setSingleStep(0.005)
        self._startJuliaX.setDecimals(4)
        self._startJuliaX.setMinimumWidth(80)
        self._startJuliaX.setEnabled(self._juliaV)
        self._startJuliaX.setValue(float(self._juliaX))

        self._startJuliaY = QDoubleSpinBox()
        self._startJuliaY.setRange(-4.0, 4.0)
        self._startJuliaY.setSingleStep(0.005)
        self._startJuliaY.setDecimals(4)
        self._startJuliaY.setMinimumWidth(80)
        self._startJuliaY.setEnabled(self._juliaV)
        self._startJuliaY.setValue(float(self._juliaY))

        self._iterationsLimit = QSpinBox(self)
        self._iterationsLimit.setRange(0, 1000)
        self._iterationsLimit.setValue(self._iterationsV)

        self._colorDensity = QSpinBox(self)
        self._colorDensity.setRange(0, 100)
        self._colorDensity.setMinimumWidth(80)
        self._colorDensity.setValue(self._colord)
        self._colorDensity.valueChanged.connect(self.__emitColorDensity)

        self._colormapSize = QSpinBox(self)
        self._colormapSize.setRange(2, 1024)
        self._colormapSize.setValue(self._colors)
        self._colormapSize.valueChanged.connect(self.__emitColorMapSize)

        self._smoothColoring = QCheckBox(self)
        self._smoothColoring.setChecked(self._smooth)
        self._smoothColoring.stateChanged.connect(self.__emitSmoothColoring)

        self._julia = QCheckBox(self)
        self._julia.setChecked(self._juliaV)
        self._julia.stateChanged.connect(self.__showJuliaOptions)

        self._gradientEditorButton = QPushButton("Color Editor", self)
        self._gradientEditorButton.clicked.connect(self.__showGradientEditor)

    def __createLayout(self):
        optionsLayout = QVBoxLayout()

        group = QGroupBox("Options")
        layout = QFormLayout()
        layout.setLabelAlignment(Qt.AlignRight)
        layout.addRow("Presets:", self._presets)
        layout.addRow("Iterations:", self._iterationsLimit)
        layout.addRow("Color Density:", self._colorDensity)
        layout.addRow("Colormap Size:", self._colormapSize)
        layout.addRow("Smooth Coloring:", self._smoothColoring)
        layout.addRow("Switch to Julia:", self._julia)
        layout.addRow("Center X:", self._startJuliaX)
        layout.addRow("Y:", self._startJuliaY)
        group.setLayout(layout)
        optionsLayout.addWidget(group)
        optionsLayout.addWidget(self._gradientEditorButton, 0, Qt.AlignHCenter)
        optionsLayout.addStretch()

        self.setLayout(optionsLayout)

    def __showJuliaOptions(self, state):
        state = False if state == 0 else True
        self._startJuliaX.setEnabled(state)
        self._startJuliaY.setEnabled(state)
        self.juliaToggle.emit(state, True)

    def __showGradientEditor(self):
        if self._gradientDialog == None:
            self._gradientDialog = GradientDialog(self)
            self._gradientDialog.resize(400, 200)
            self._gradientDialog.show()
            self._gradientDialog.setGradientStops(self._gradientStops)
            self._gradientDialog.hide()
            self._gradientDialog.gradientChanged.connect(self.__setGradientStops)

        if self._gradientDialog.isVisible() == False:
            self._gradientDialog.show()
        else:
            self._gradientDialog.hide()

    def __setGradientStops(self, stops):
        self._gradientStops = stops
        self.gradientStopsChanged.emit(self._gradientStops, True)

    def __emitColorDensity(self, value):
        self.colorDensityChanged.emit(value, True)

    def __emitColorMapSize(self, value):
        self.colormapSizeChanged.emit(value, True)

    def __emitSmoothColoring(self, value):
        self.smoothColoringToggle.emit(value, True)

    def emitStartData(self):
        print("emit start")
        self.emitData()
        self.start.emit()

    def emitData(self):
        self.typeChanged.emit(self._presets.currentIndex())
        self.iterationsChanged.emit(self._iterationsLimit.value())
        self.gradientStopsChanged.emit(self._gradientStops, False)
        self.colorDensityChanged.emit(self._colorDensity.value(), False)
        self.colormapSizeChanged.emit(self._colormapSize.value(), False)
        self.juliaXChanged.emit(self._startJuliaX.value())
        self.juliaYChanged.emit(self._startJuliaY.value())
        self.juliaToggle.emit(self._julia.checkState(), False)
        self.smoothColoringToggle.emit(self._smoothColoring.checkState(), False)

    def setJulia(self, x, y):
        self._startJuliaX.setValue(x)
        self._startJuliaY.setValue(y)


if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)

    ops = MandelbrotOptions()
    qApp.aboutToQuit.connect(ops.saveSettings)
    ops.show()

    sys.exit(app.exec_())
