#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


from PyQt5.QtCore import *
from PyQt5.QtGui import *

import plugins.abstractfractal as af
from plugins.mandelbrot.mandelbrot import MandelbrotWidget

from .mandelbrotoptions import MandelbrotOptions


class MandelbrotExplorer(af.AbstractFractalExplorer):
    start = pyqtSignal()
    stop = pyqtSignal()
    stopped = pyqtSignal()
    saveToFile = pyqtSignal(QString)
    saveSettings = pyqtSignal()
    zoomIn = pyqtSignal()
    zoomOut = pyqtSignal()
    state = pyqtSignal(str)

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.__createWidgets()

        # from outside
        self.start.connect(self._fractalOptions.emitStartData)
        self.stop.connect(self._fractalWidget.stop)
        self.saveToFile.connect(self._fractalWidget.saveTo)
        self.zoomIn.connect(self._fractalWidget.zoomIn)
        self.zoomOut.connect(self._fractalWidget.zoomOut)
        self.saveSettings.connect(self._fractalOptions.saveSettings)

        # to outside
        self._fractalWidget.information.connect(self.state)
        self._fractalWidget.stopped.connect(self.stopped)

        # from options to widget
        self._fractalOptions.iterationsChanged.connect(
            self._fractalWidget.setIterations
        )
        self._fractalOptions.colorDensityChanged.connect(
            self._fractalWidget.setColorDensity
        )
        self._fractalOptions.colormapSizeChanged.connect(
            self._fractalWidget.setColorMapSize
        )
        self._fractalOptions.juliaXChanged.connect(self._fractalWidget.setJuliaX)
        self._fractalOptions.juliaYChanged.connect(self._fractalWidget.setJuliaY)
        self._fractalOptions.gradientStopsChanged.connect(
            self._fractalWidget.setGradientStops
        )
        self._fractalOptions.smoothColoringToggle.connect(
            self._fractalWidget.setSmoothColoring
        )
        self._fractalOptions.juliaToggle.connect(self._fractalWidget.setJulia)
        self._fractalOptions.typeChanged.connect(self._fractalWidget.setType)
        self._fractalOptions.start.connect(self._fractalWidget.start)

        # from widget to options
        self._fractalWidget.juliaCenterChanged.connect(self._fractalOptions.setJulia)

        # give all data from settings file to widget
        self._fractalOptions.emitStartData()

    def __createWidgets(self):
        self._fractalWidget = MandelbrotWidget()
        self._fractalOptions = MandelbrotOptions()

        layout = QHBoxLayout()
        layout.addWidget(self._fractalWidget)
        layout.addWidget(self._fractalOptions)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)


if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)
    exp = MandelbrotExplorer()
    exp.resize(800, 600)
    qApp.aboutToQuit.connect(exp.saveSettings)
    exp.show()

    sys.exit(app.exec_())
