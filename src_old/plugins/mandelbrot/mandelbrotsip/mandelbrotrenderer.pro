TEMPLATE        = lib
CONFIG          += qt warn_on release
HEADERS         = mandelbrotrenderer.h
SOURCES         = mandelbrotrenderer.cpp
TARGET          = mandelbrotrenderer

unix:UI_DIR             = .ui
unix:MOC_DIR            = .moc
unix:OBJECTS_DIR        = .obj

win32:UI_DIR            = .tmp
win32:MOC_DIR           = .tmp
win32:OBJECTS_DIR       = .tmp