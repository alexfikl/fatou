/* *******************************************************************
 * *
 ** Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
 **
 ** This file is part of Fatou
 **
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **
 ***********************************************************************/


#include <QtGui>

#include <math.h>

#include "mandelbrotrenderer.h"

MandelbrotRenderer::MandelbrotRenderer()
    : QObject()
{
    m_scaleFactor = 0.0078125;
    m_centerX = -0.637011;
    m_centerY = 0.0;
    m_juliaX = 0.0;
    m_juliaY = 0.0;

    m_iterations = 50;
    m_colorMapSize = 512;
    m_smoothColoring = false;
    m_julia = false;
}

MandelbrotRenderer::~MandelbrotRenderer()
{
}

void MandelbrotRenderer::render(QSize size, int type)
{
    qDebug() << "rendering";
    qDebug() << "center" << m_centerX << m_centerY;
    qDebug() << "julia" << m_juliaX << m_juliaY;

    int halfWidth = size.width() / 2;
    int halfHeight = size.height() / 2;

    m_abort = false;
    m_iterationsList.clear();
    m_realIterationsList.clear();

    for(int y = -halfHeight; y < halfHeight; ++y)
    {
        if(m_abort)
            return;

        double ay = m_centerY + (y * m_scaleFactor);
        for(int x = -halfWidth; x < halfWidth; ++x)
        {
            double ax = m_centerX + (x * m_scaleFactor);
            int its = iterate(ax, ay, type);

            m_iterationsList << its;
            m_realIterationsList << smoothIterations(its, ax, ay);
        }
    }

    color(size);
}

void MandelbrotRenderer::color(QSize size)
{
    if(m_realIterationsList.isEmpty() || m_iterationsList.isEmpty())
        return;

    int halfWidth = size.width() / 2;
    int halfHeight = size.height() / 2;
    QImage image(size, QImage::Format_RGB32);
    int i = 0;

    for(int y = -halfHeight; y < halfHeight; ++y)
    {
        uint *scanLine = reinterpret_cast<uint *>(image.scanLine(y + halfHeight));
        for(int x = -halfWidth; x < halfWidth; ++x)
        {
            *scanLine++ = getColorFor(i);
            ++i;
        }
    }

    emit completed(image, m_scaleFactor);
}

double MandelbrotRenderer::smoothIterations(int its, double zx, double zy)
{
    // FIXME: this will probably not work. figure out how to use the thing from
    // newton. seemed to somewhat work;
    return its - (log(log(4)) - log(log(sqrt(zx * zx + zy * zy)))) / log(2);
}

uint MandelbrotRenderer::getColorFor(int i)
{
    if(m_iterationsList.at(i) <= 0)
        return qRgb(0, 0, 0);
    else
    {
        int its;
        if(m_smoothColoring)
            its = (int)(m_colorDensity * m_realIterationsList[i]);
        else
            its = m_colorDensity * m_iterationsList[i];
        return m_colormap.at(its % m_colorMapSize);
    }
}

int MandelbrotRenderer::iterate(double x, double y, int type)
{
    double zx, zy, cx, cy;
    int its;

    if(m_julia)
    {
        zx = x;
        zy = y;
        cx = m_juliaX;
        cy = m_juliaY;
    }
    else
    {
        zx = zy = 0.0;
        cx = x;
        cy = y;
    }

    switch(type)
    {
        case 0:
            its = mandelbrot2(zx, zy, cx, cy);
            break;
        case 1:
            its = mandelbrot3(zx, zy, cx, cy);
            break;
        case 2:
            its = mandelbrot4(zx, zy, cx, cy);
            break;
        default:
            its = 0;
            break;
    }

    return its;
}

int MandelbrotRenderer::mandelbrot2(double zx, double zy, double cx,  double cy)
{
    uint i = 0;
    double temp;
    double limit = 4.0;

    while((zx * zx + zy * zy) < limit && i < m_iterations)
    {
        /* (x + iy)^2 = x^2 - y^2 + (2.0xy)i */
        temp = zx * zx - zy * zy + cx;
        zy = 2.0 * zx * zy + cy;
        zx = temp;
        ++i;
    }

    return i;
}

int MandelbrotRenderer::mandelbrot3(double zx, double zy, double cx,  double cy)
{
    uint i = 0;
    double temp;
    double limit = 4.0;

    while((zx * zx + zy * zy) < limit && i < m_iterations)
    {
        /* (x + iy)^3 = x^3 - 3xy^2 + (3x^2y - y^3)i */
        temp = zx * zx * zx - 3.0 * zx * zy * zy + cx;
        zy = 3.0 * zx * zx * zy - zy * zy * zy + cy;
        zx = temp;
        ++i;
    }

    return i;
}

int MandelbrotRenderer::mandelbrot4(double zx, double zy, double cx,  double cy)
{
    uint i = 0;
    double temp;
    double limit = 4.0;

    while((zx * zx + zy * zy) < limit && i < m_iterations)
    {
        /* (x+yi)^4 = x^4 - 6x^2y^2 + y^4 + (4x^3y - 4xy^3)i */
        temp = zx * zx * zx * zx - 6.0 * zx * zx * zy * zy + zy * zy * zy * zy + cx;
        zy = 4.0 * zx * zx * zx * zy - 4.0 * zx * zy * zy * zy + cy;
        zx = temp;
        ++i;
    }

    return i;
}

void MandelbrotRenderer::setIterations(int iterations)
{
    m_iterations = iterations;
}

void MandelbrotRenderer::setCenterX(double x)
{
    m_centerX = x;
}

void MandelbrotRenderer::setCenterY(double y)
{
    m_centerY = y;
}

void MandelbrotRenderer::setJuliaX(double x)
{
    this->m_juliaX = x;
}

void MandelbrotRenderer::setJuliaY(double y)
{
    this->m_juliaY = y;
}

void MandelbrotRenderer::setColorMapSize(int size)
{
    m_colorMapSize = size;
}

void MandelbrotRenderer::setColorDensity(int density)
{
    m_colorDensity = density;
}

void MandelbrotRenderer::setGradientStops(QGradientStops stops)
{
    // Gradient generation
    QImage gradientImage(m_colorMapSize, 1, QImage::Format_ARGB32);
    QPainter painter(&gradientImage);

    QLinearGradient gradient(0, 0, m_colorMapSize, 0);
    gradient.setStops(stops);

    painter.fillRect(gradientImage.rect(), gradient);

    // Colormap filling
    if(m_colorMapSize != m_colormap.size())
        m_colormap.clear();

    for(uint i = 0; i < m_colorMapSize; ++i)
        m_colormap << gradientImage.pixel(i, 0);
}

void MandelbrotRenderer::setSmoothColoring(bool coloring)
{
    m_smoothColoring = coloring;
}

void MandelbrotRenderer::setJulia(bool julia)
{
    m_julia = julia;
}

void MandelbrotRenderer::setScale(double scale)
{
    m_scaleFactor = scale;
}

void MandelbrotRenderer::setCenterCoordinates(double x, double y)
{
    setCenterX(x);
    setCenterY(y);
}

void MandelbrotRenderer::abort()
{
    m_abort = true;
}

double MandelbrotRenderer::centerX()
{
    return m_centerX;
}

double MandelbrotRenderer::centerY()
{
    return m_centerY;
}

double MandelbrotRenderer::scale()
{
    return m_scaleFactor;
}

bool MandelbrotRenderer::julia()
{
    return m_julia;
}