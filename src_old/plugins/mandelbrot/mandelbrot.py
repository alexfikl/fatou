#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

from MandelbrotRenderer import MandelbrotRenderer
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class MandelbrotWidget(QWidget):
    juliaCenterChanged = pyqtSignal(float, float)
    information = pyqtSignal(str)
    stopped = pyqtSignal()
    render = pyqtSignal(QSize, int)
    color = pyqtSignal(QSize)

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self._zoomFactor = {"in": 0.8, "out": 1.25}
        self._scrollStep = 20
        self._pixmap = QPixmap()
        self._pixmapOffset = QPointF()
        self._lastDragPosition = QPointF()
        self._currentScale = 0.0078125
        self._type = 0

        # this logic is probably flawed
        # make a thread -> move the object to it
        # when we tell the object to render it starts the thread
        # when the rendering is complete it stops it
        self._thread = QThread()
        self._renderer = MandelbrotRenderer()
        self._renderer.moveToThread(self._thread)
        self.render.connect(self._thread.start)
        self.color.connect(self._thread.start)
        self.render.connect(self._renderer.render)
        self.color.connect(self._renderer.color)
        self._renderer.completed.connect(self.__updatePixmap)
        self._renderer.completed.connect(self._thread.quit)
        self._renderer.completed.connect(self.stopped)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.fillRect(self.rect(), Qt.black)

        if self._pixmap.isNull():
            painter.setPen(Qt.white)
            painter.drawText(
                self.rect(), Qt.AlignCenter, "Rendering initial image, please wait."
            )
            return

        if self._currentScale == self._renderer.scale():
            painter.drawPixmap(self._pixmapOffset, self._pixmap)
        else:
            scaleFactor = self._renderer.scale() / self._currentScale
            newWidth = int(self._pixmap.width() * scaleFactor)
            newHeight = int(self._pixmap.height() * scaleFactor)
            newX = self._pixmapOffset.x() + (self._pixmap.width() - newWidth) / 2
            newY = self._pixmapOffset.y() + (self._pixmap.height() - newHeight) / 2

            painter.save()
            painter.translate(newX, newY)
            painter.scale(scaleFactor, scaleFactor)
            exposed = painter.matrix().inverted()[0]
            exposed = exposed.mapRect(self.rect().adjusted(-1.0, -1.0, 1.0, 1.0))
            painter.drawPixmap(exposed, self._pixmap, exposed)
            painter.restore()

    def resizeEvent(self, event):
        self._renderer.abort()
        self.render.emit(self.size(), self._type)

    def keyPressEvent(self, event):
        key = event.key()

        if key == Qt.Key_Plus:
            self.__zoom(self._zoomFactor["in"])
        elif key == Qt.Key_Minus:
            self.__zoom(self._zoomFactor["out"])
        elif key == Qt.Key_Left:
            self.__scroll(-self._step, 0)
        elif key == Qt.Key_Right:
            self.__scroll(self._step, 0)
        elif key == Qt.Key_Down:
            self.__scroll(0, -self._step)
        elif key == Qt.Key_Up:
            self.__scroll(0, self._step)
        else:
            QWidget.keyPressEvent(self, event)

    def wheelEvent(self, event):
        numDegrees = event.delta() / 8
        numSteps = numDegrees / 15.0
        self.__zoom(self._zoomFactor["in"] ** numSteps)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            # change the coordinates in options when we click on something
            # and the julia option is on

            self._lastDragPosition = event.pos()
        if event.button() == Qt.RightButton and self._julia:
            posx = event.pos().x() - self.width() / 2
            posy = event.pos().y() - self.height() / 2
            x = self._renderer.centerX() + posx * self._currentScale
            y = self._renderer.centerY() + posy * self._currentScale
            self.juliaCenterChanged.emit(x, y)

    def mouseMoveEvent(self, event):
        self._renderer.abort()
        if event.buttons() == Qt.LeftButton:
            self._pixmapOffset += event.pos() - self._lastDragPosition
            self._lastDragPosition = event.pos()
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self._pixmapOffset += event.pos() - self._lastDragPosition
            self._lastDragPosition = QPoint()

            deltaX = (self.width() - self._pixmap.width()) / 2 - self._pixmapOffset.x()
            deltaY = (
                self.height() - self._pixmap.height()
            ) / 2 - self._pixmapOffset.y()
            self.__scroll(deltaX, deltaY)

    def __updatePixmap(self, image, scale):
        if not self._lastDragPosition.isNull():
            return

        self._pixmap = QPixmap.fromImage(image)
        self._pixmapOffset = QPoint()
        self._lastDragPosition = QPoint()
        self.update()

    def saveTo(self, filename):
        image = self._pixmap.toImage()
        image.save(filename)

    def start(self):
        self.render.emit(self.size(), self._type)

    def stop(self):
        self._renderer.abort()

    def zoomIn(self):
        self.__zoom(self._zoomFactor["in"])

    def zoomOut(self):
        self.__zoom(self._zoomFactor["out"])

    def __zoom(self, zoom):
        # update scale
        self._currentScale *= zoom

        # update widget
        self.update()

        # tell the renderer the new scale and start rendering
        self._renderer.setScale(self._currentScale)
        self.render.emit(self.size(), self._type)

        x = self._renderer.centerX()
        y = self._renderer.centerY()
        i = f"Center: ({x:.3f}, {y:.3f}) Scale: {self._currentScale:.3f}"
        self.information.emit(i)

    def __scroll(self, deltaX, deltaY):
        # get new coordinates
        x = self._renderer.centerX() + deltaX * self._currentScale
        y = self._renderer.centerY() + deltaY * self._currentScale

        # send coordinates to renderer
        self._renderer.setCenterCoordinates(x, y)
        self.update()

        # emit the new information
        i = f"Center: ({x:.3f}, {y:.3f}) Scale: {self._currentScale:.3f}"
        self.information.emit(i)

        # tell the render to render with the new center
        self.render.emit(self.size(), self._type)

    def setIterations(self, its):
        self._renderer.setIterations(its)

    def setColorDensity(self, density, color):
        self._renderer.setColorDensity(density)
        if color:
            self.color.emit(self.size())

    def setCenterX(self, x):
        self._renderer.setCenterX(x)

    def setCenterY(self, y):
        self._renderer.setCenterY(y)

    def setJuliaX(self, x):
        self._renderer.setJuliaX(x)

    def setJuliaY(self, y):
        self._renderer.setJuliaY(y)

    def setColorMapSize(self, size, color):
        self._renderer.setColorMapSize(size)
        if color:
            self.color.emit(self.size())

    def setGradientStops(self, stops, color):
        self._renderer.setGradientStops(stops)
        if color:
            self.color.emit(self.size())

    def setJulia(self, julia, send):
        self._julia = julia
        if send:
            self._renderer.setJulia(julia)

    def setSmoothColoring(self, smooth, color):
        self._renderer.setSmoothColoring(False if smooth == 0 else True)
        if color:
            self.color.emit(self.size())

    def setType(self, mtype):
        self._type = mtype


if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)
    exp = MandelbrotWidget()
    exp.setSmoothColoring(1)
    exp.resize(400, 300)
    exp.show()

    sys.exit(app.exec_())
