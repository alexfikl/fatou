"""Contains Pattern Chooser for Elementary Cellular Automata.
"""

#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


import PyQt5.QtGui as qg
from PyQt5.QtCore import QRect, QSize, Qt, pyqtSignal

from .flowlayout import FlowLayout


class Pattern(qg.QWidget):
    """One option for the pattern chooser."""

    patternChanged = pyqtSignal(int, int)

    def __init__(self, index, rule, parent=None):
        qg.QWidget.__init__(self, parent)

        self._index = index
        self._indexbin = [int(i) for i in bin(index)[2:].zfill(3)]
        self._rule = rule
        self._positions = []

        self.setBackgroundRole(qg.QPalette.Base)

    def mousePressEvent(self, ev):
        """Implemented virtual function."""
        if ev.button() == Qt.LeftButton:
            # toggle rect
            self._rule = 0 if self._rule else 1
            self.patternChanged.emit(self._index, self._rule)
            self.update()

    def resizeEvent(self, _):
        """Implemented virtual function."""
        self._positions = []
        self._positions.append((self.width() / 3 + 2, 5))
        self._positions.append((7, self.height() / 2))
        self._positions.append((self.width() / 3 + 2, self.height() / 2))
        self._positions.append((2 * self.width() / 3 - 4, self.height() / 2))

    def paintEvent(self, _):
        """Implemented virtual function."""
        painter = qg.QPainter(self)
        pen = qg.QPen()
        pen.setBrush(Qt.white)
        pen.setStyle(Qt.SolidLine)
        pen.setWidth(2)
        pen.setJoinStyle(Qt.MiterJoin)
        painter.setPen(pen)
        painter.setRenderHint(qg.QPainter.Antialiasing)

        rect = QRect(0, 0, self.width() / 3 - 10, self.height() / 2 - 10)

        patterns = [self._rule] + self._indexbin
        for i in range(4):
            painter.save()
            painter.translate(self._positions[i][0], self._positions[i][1] - 1)
            if patterns[i]:
                painter.setBrush(Qt.black)
            else:
                painter.setBrush(Qt.NoBrush)
            painter.drawRect(rect)
            painter.restore()

    def heightForWidth(self, width):
        """Implemented virtual function."""
        return int((width / 3) * 2)

    def sizeHint(self):
        """Implemented virtual function."""
        return QSize(60, 40)

    def minimumSizeHint(self):
        """Implemented virtual function."""
        return QSize(60, 40)


class PatternChooser(qg.QWidget):
    """Composed of 8 Patterns, one for every possible combination of 2 values
    (0 and 1) in 3 spaces.
    """

    ruleChanged = pyqtSignal(int)

    def __init__(self, rule, parent=None):
        qg.QWidget.__init__(self, parent)
        self._rule = [int(i) for i in bin(rule)[2:].zfill(8)]

        layout = FlowLayout()
        for index, item in enumerate(self._rule):
            pat = Pattern(index, item, self)
            pat.patternChanged.connect(self.patternChanged)
            layout.addWidget(pat)
        self.setLayout(layout)

    def getRule(self):
        """Returns the current rule as expressed by the pattern."""
        rule = int("".join([str(i) for i in self._rule]), 2)
        return rule

    def patternChanged(self, index, pattern):
        """Slot for changed rules."""
        self._rule[index] = pattern
        self.ruleChanged.emit(self.getRule())


if __name__ == "__main__":
    import sys

    APP = qg.QApplication(sys.argv)
    CH = PatternChooser(110)
    CH.resize(400, 40)
    CH.show()

    sys.exit(APP.exec_())
