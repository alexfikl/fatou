#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


from PyQt5.QtCore import *
from PyQt5.QtGui import *

from . import cellpoolmodel as cm


class AutomataWidget(QWidget):
    status = pyqtSignal(str)
    stopped = pyqtSignal()

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        # init some values
        self._speed = 700
        self._maxCells = 0
        self._cellSize = 10
        self._showGrid = True

        # init some more
        self._cellPoolModel = cm.CellPoolModel(30, self)
        self._cellPoolModel.setAutomataType(0)
        self._cellPoolModel.setAutoResize(False)
        self._cellPoolModel.resized.connect(self.__scaleCells)

        # my head is killing me
        self._grid = QTableView(self)
        self._grid.setModel(self._cellPoolModel)
        self._grid.setShowGrid(True)
        self.__scaleCells()

        self._grid.horizontalHeader().setVisible(False)
        self._grid.verticalHeader().setVisible(False)
        self._grid.clicked.connect(self.__cellClicked)

        layout = QVBoxLayout()
        layout.addWidget(self._grid)
        self.setLayout(layout)

        self._simulationTimer = QTimer(self)
        self._simulationTimer.timeout.connect(self.__simulate)

    def wheelEvent(self, ev):
        newSize = self._cellSize + ev.delta() / 40
        if newSize > 3 and newSize < 100:
            self._cellSize = newSize
            self.__scaleCells()
            size = False if self._cellSize <= 7 else True
            self._grid.setShowGrid(self._showGrid and size)

    def zoomIn(self):
        if self._cellSize < 100:
            self._cellSize += 2
            size = False if self._cellSize <= 7 else True
            self._grid.setShowGrid(self._showGrid and size)
            self.__scaleCells()

    def zoomOut(self):
        if self._cellSize > 5:
            self._cellSize -= 2
            size = False if self._cellSize <= 7 else True
            self._grid.setShowGrid(self._showGrid and size)
            self.__scaleCells()

    def startSimulation(self):
        if self._cellPoolModel.automataType() == cm.CellPoolModel.WOLFRAM:
            self._cellPoolModel.clear()
        if self._cellPoolModel.population():
            self._simulationTimer.start(self._speed)
        else:
            self.stopped.emit()

    def stopSimulation(self):
        self._simulationTimer.stop()

    def clearPool(self):
        self._cellPoolModel.clear()
        self.stopSimulation()

    def setSpeed(self, newSpeed):
        self._speed = newSpeed

    def setSize(self, newSize):
        self._cellPoolModel.setSize(newSize)

    def toggleAutoResize(self, newAutoResize):
        self._cellPoolModel.setAutoResize(False if not newAutoResize else True)

    def toggleShowGrid(self, newShowGrid):
        self._showGrid = False if not newShowGrid else True
        size = False if self._cellSize < 7 else True
        self._grid.setShowGrid(self._showGrid and size)

    def toggleRandomize(self, random):
        self._cellPoolModel.setRandomize(False if not random else True)

    def setType(self, atype):
        self._cellPoolModel.setAutomataType(atype)

    def setRule(self, rule):
        self._cellPoolModel.setRule(rule)

    def __scaleCells(self):
        for i in range(self._cellPoolModel.size()):
            self._grid.setRowHeight(i, self._cellSize)
            self._grid.setColumnWidth(i, self._cellSize)
        if (self._cellPoolModel.size() * self._cellSize) > self.width():
            self.zoomOut()

    def __simulate(self):
        self._cellPoolModel.simulate()
        gen = self._cellPoolModel.generation()
        status = "Generation: %d" % (gen)
        if self._cellPoolModel.automataType() == cm.CellPoolModel.GAMEOFLIFE:
            currentPopulation = self._cellPoolModel.population()
            self._maxCells = max(self._maxCells, currentPopulation)

            if not currentPopulation:
                self.stopped.emit()
                self._simulationTimer.stop()
            status += " Max Cells: %d" % (self._maxCells)
        else:
            if self._cellPoolModel.size() == gen + 1:
                self.stopped.emit()
                self._simulationTimer.stop()
        self.status.emit(status)

    def __cellClicked(self, index):
        self._cellPoolModel.toggle(index.row(), index.column())
