"""Module that contains the main cellular automata plugin class.
"""


#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


import PyQt5.QtGui as qg
from PyQt5.QtCore import pyqtSignal

from plugins.abstractfractal import AbstractFractalExplorer

from .automataoptions import AutomataOptions
from .automatawidget import AutomataWidget


class AutomataExplorer(AbstractFractalExplorer):
    """Main Cellular Automata class."""

    start = pyqtSignal()
    stop = pyqtSignal()
    saveSettings = pyqtSignal()
    stopped = pyqtSignal()
    state = pyqtSignal(str)
    zoomIn = pyqtSignal()
    zoomOut = pyqtSignal()

    def __init__(self, parent=None):
        AbstractFractalExplorer.__init__(self, parent)

        self._fractal = AutomataWidget(self)
        self._opts = AutomataOptions(self)
        self.__createLayout()

        # connect outside signals
        self.start.connect(self._opts.emitStartData)
        self.saveSettings.connect(self._opts.saveSettings)
        self.stop.connect(self._fractal.stopSimulation)
        self.zoomIn.connect(self._fractal.zoomIn)
        self.zoomOut.connect(self._fractal.zoomOut)

        self._fractal.status.connect(self.state)
        self._fractal.stopped.connect(self.stopped)

        # connect all singals from options
        self._opts.start.connect(self._fractal.startSimulation)
        self._opts.clear.connect(self._fractal.clearPool)
        self._opts.typeChanged.connect(self._fractal.setType)
        self._opts.speedChanged.connect(self._fractal.setSpeed)
        self._opts.sizeChanged.connect(self._fractal.setSize)
        self._opts.patternChanged.connect(self._fractal.setRule)
        self._opts.autoResizeToggle.connect(self._fractal.toggleAutoResize)
        self._opts.showGridToggle.connect(self._fractal.toggleShowGrid)
        self._opts.randomizeToggle.connect(self._fractal.toggleRandomize)

        self._opts.emitData(True)

    def __createLayout(self):
        layout = qg.QHBoxLayout(self)
        layout.addWidget(self._fractal)
        layout.addWidget(self._opts)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)


if __name__ == "__main__":
    import sys

    APP = qg.QApplication(sys.argv)
    LIFE = AutomataExplorer()
    qg.qApp.aboutToQuit.connect(LIFE.saveSettings)

    LIFE.resize(600, 400)
    LIFE.show()

    sys.exit(APP.exec_())
