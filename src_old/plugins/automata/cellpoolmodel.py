#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


import random

import PyQt5.QtGui as qg
from PyQt5.QtCore import QAbstractTableModel, QSize, Qt, QVariant, pyqtSignal


class CellPoolModel(QAbstractTableModel):
    resized = pyqtSignal()
    GAMEOFLIFE = 0
    WOLFRAM = 1

    def __init__(self, size, parent=None):
        QAbstractTableModel.__init__(self, parent)
        self._size = size
        self._cells = [[False] * self._size for i in range(self._size)]
        self._rule = 0
        self._type = 0
        self._generation = 0
        self._autoresize = False
        self._randomize = False
        self._grid = [0, 0, self._size, self._size]

    def __resize(self):
        oldsize = len(self._cells)
        newcells = [[False] * self._size for i in range(self._size)]

        # get the number of cells to add to left and right/ up and down
        diffr = abs(self._size - oldsize)
        if diffr % 2:
            diffl = diffr // 2
            diffr = diffr // 2 + 1
        else:
            diffr = diffl = diffr // 2

        # if the new size is larger insert the old cells in the middle
        # otherwise slice the old cells to fit
        if self._size > oldsize:
            for i in range(oldsize):
                newcells[i + diffl][diffl:diffr] = self._cells[i]
        else:
            for i in range(self._size):
                newcells[i] = self._cells[i + diffl][diffl : oldsize - diffr]

        self._cells = newcells
        self.reset()
        self.resized.emit()

    def population(self):
        return sum([sum(row) for row in self._cells])

    def __isAlive(self, row, column):
        if row < 0 or column < 0 or row >= self._size or column >= self._size:
            return False

        return self._cells[row][column]

    def __neighborsAlive(self, row, column):
        if self._type == CellPoolModel.WOLFRAM:
            return -1

        alive = self.__isAlive(row - 1, column + 1)
        alive += self.__isAlive(row - 1, column)
        alive += self.__isAlive(row - 1, column - 1)
        alive += self.__isAlive(row, column + 1)
        alive += self.__isAlive(row, column - 1)
        alive += self.__isAlive(row + 1, column + 1)
        alive += self.__isAlive(row + 1, column)
        alive += self.__isAlive(row + 1, column - 1)

        return alive

    def simulate(self):
        switch = {0: self.__simulateGameOfLife, 1: self.__simulateWolfram}

        switch.get(self._type, lambda: None)()
        self.reset()
        self._generation += 1

    def __simulateGameOfLife(self):
        nextGen = [[False] * self._size for i in range(self._size)]
        resize = False
        for row in range(self._size):
            for col in range(self._size):
                neighbors = self.__neighborsAlive(row, col)

                if neighbors == 2:
                    if self._cells[row][col]:
                        nextGen[row][col] = True
                        r = row == 0 or row == (self._size - 1)
                        c = col == 0 or col == (self._size - 1)
                        if r or c:
                            resize = True
                elif neighbors == 3:
                    nextGen[row][col] = True
                    r = row == 0 or row == (self._size - 1)
                    c = col == 0 or col == (self._size - 1)
                    if r or c:
                        resize = True

        self._cells = nextGen
        if resize:
            self.setSize(self._size + 10)

    def __simulateWolfram(self):
        def getNewCell(i):
            index = 4 * self._cells[self._generation][(i - 1) % self._size]
            index += 2 * self._cells[self._generation][i]
            index += int(self._cells[self._generation][(i + 1) % self._size])

            # print i, self._rule, index, self._rule[7 - index]
            return self._rule[7 - index]

        self._cells[self._generation + 1] = list(
            map(getNewCell, list(range(self._size)))
        )

    def generation(self):
        return self._generation

    def toggle(self, row, column):
        if self._type == CellPoolModel.WOLFRAM:
            return

        self._cells[row][column] = False if self._cells[row][column] else True
        self._generation = 0

    def size(self):
        return self._size

    def setSize(self, size):
        if self._size != size:
            self._size = size
            self.__resize()

    def automataType(self):
        return self._type

    def setAutomataType(self, newtype):
        if self._type != newtype:
            self._type = newtype
            self.clear()
            self._generation = 0

    def rule(self):
        return self._rule

    def setRule(self, rule):
        self._rule = rule if 0 <= rule < 256 else 255
        self._rule = [int(i) for i in bin(self._rule)[2:].zfill(8)]

    def autoresize(self):
        return self._autoresize

    def setRandomize(self, random):
        self._randomize = random

    def setAutoResize(self, ar):
        self._autoresize = True if ar else False

    def clear(self):
        self._cells = [[False] * self._size for i in range(self._size)]
        if self._type == CellPoolModel.WOLFRAM:
            print("wolfram")
            if not self._randomize:
                self._cells[0][self._size // 2] = True
            else:
                self._cells[0] = [random.randint(0, 1) for i in range(self._size)]
        self._generation = 0
        self.reset()

    def data(self, index, role):
        if index.isValid() and role == Qt.BackgroundRole:
            if self.__isAlive(index.row(), index.column()):
                return qg.QBrush(qg.QColor("white"))
            else:
                return qg.QBrush(qg.QColor("black"))
        if role == Qt.SizeHintRole:
            return QSize(10, 10)

        return QVariant()

    def headerData(self, section, orientation, role):
        return QVariant()

    def rowCount(self, parent):
        return self._size

    def columnCount(self, parent):
        return self._size

    def cellStatusChanged(self, row, column, status):
        indexToUpdate = self.index(row, column)
        self.dataChanged.emit(indexToUpdate, indexToUpdate)
