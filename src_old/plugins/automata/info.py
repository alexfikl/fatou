"""Information about the current plugin.
"""

NAME = "Cellular Automata"

FRACTALS = ["Conway's Game of Life", "Elementary Cellular Automata"]
