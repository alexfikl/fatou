#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


from PyQt5.QtCore import *
from PyQt5.QtGui import *

from plugins.abstractfractal import AbstractFractalOptions

from .info import FRACTALS
from .patternchooser import PatternChooser


class AutomataOptions(AbstractFractalOptions):
    typeChanged = pyqtSignal(int)
    speedChanged = pyqtSignal(int)
    sizeChanged = pyqtSignal(int)
    patternChanged = pyqtSignal(int)
    autoResizeToggle = pyqtSignal(int)
    showGridToggle = pyqtSignal(int)
    randomizeToggle = pyqtSignal(int)
    start = pyqtSignal()
    clear = pyqtSignal()

    def __init__(self, parent=None):
        AbstractFractalOptions.__init__(self, parent)

        self.__readSettings()
        self.__createWidgets()
        self.__addLayout()

        self.setMinimumWidth(160)
        self.setMaximumWidth(320)

    def __readSettings(self):
        settings = QSettings("fatou", "fatou")

        settings.beginGroup("automata")
        self._current = settings.value("current", 0).toInt()[0]
        self._speedV = settings.value("speed", 700).toInt()[0]
        self._sizeV = settings.value("grid-size", 30).toInt()[0]
        self._autoresizeV = settings.value("autoresize", 0).toInt()[0]
        self._showgridV = settings.value("show-grid", 2).toInt()[0]
        self._randomV = settings.value("wolfram-random", False).toBool()
        self._rule = settings.value("rule", 110).toInt()[0]
        settings.endGroup()

    def saveSettings(self):
        settings = QSettings("fatou", "fatou")

        settings.beginGroup("automata")
        settings.setValue("current", self._type.currentIndex())
        settings.setValue("speed", self._speed.value())
        settings.setValue("grid-size", self._size.value())
        settings.setValue("autoresize", self._autoResize.checkState())
        settings.setValue("show-grid", self._showGrid.checkState())
        settings.setValue("rule", self._patterns.getRule())
        settings.setValue("wolfram-random", self._random.checkState())
        settings.endGroup()

    def __createWidgets(self):
        self._type = QComboBox(self)
        for item in FRACTALS:
            self._type.addItem(item)
        self._type.setCurrentIndex(self._current)
        self._type.activated.connect(self.__toggleShowPatterns)
        self._type.activated.connect(self.typeChanged)

        self._speed = QSpinBox(self)
        self._speed.setRange(0, 10000)
        self._speed.setValue(self._speedV)

        self._size = QSpinBox(self)
        self._size.setRange(0, 1000)
        self._size.setValue(self._sizeV)

        self._patterns = PatternChooser(self._rule, self)
        self._patterns.ruleChanged.connect(self.__setGroupTitle)

        self._autoResize = QCheckBox()
        self._autoResize.setChecked(self._autoresizeV)
        self._autoResize.stateChanged.connect(self.autoResizeToggle)

        self._showGrid = QCheckBox()
        self._showGrid.setChecked(self._showgridV)
        self._showGrid.stateChanged.connect(self.showGridToggle)

        self._random = QCheckBox()
        self._random.setEnabled(self._type.currentIndex() == 1)
        self._random.setChecked(self._randomV)
        self._random.stateChanged.connect(self.randomizeToggle)

        self._clear = QPushButton("Clear")
        self._clear.clicked.connect(self.clear)

    def __addLayout(self):
        group = QGroupBox("Game of Life Options")
        formLayout = QFormLayout(group)
        formLayout.setLabelAlignment(Qt.AlignLeft)
        formLayout.addRow("Type:", self._type)
        formLayout.addRow("Speed (ms):", self._speed)
        formLayout.addRow("Size:", self._size)
        formLayout.addRow("Autoresize:", self._autoResize)
        formLayout.addRow("Show Grid:", self._showGrid)
        formLayout.addRow("Randomize Start:", self._random)

        layout = QVBoxLayout(self)
        layout.addWidget(group)

        self._patternsGroup = QGroupBox("Rule " + str(self._rule))
        self._patternsGroup.setVisible(True if self._current == 1 else False)
        playout = QHBoxLayout(self._patternsGroup)
        playout.addWidget(self._patterns)

        layout.addWidget(self._patternsGroup)
        layout.addWidget(self._clear, 0, Qt.AlignCenter)
        layout.addStretch()

    def emitStartData(self):
        self.emitData()
        self.start.emit()

    def emitData(self, emitAll=False):
        if emitAll:
            self.typeChanged.emit(self._current)
            self.randomizeToggle.emit(self._randomV)
            self.showGridToggle.emit(self._showgridV)

        self.speedChanged.emit(self._speed.value())
        self.sizeChanged.emit(self._size.value())
        if self._type.currentIndex() == 1:
            self.patternChanged.emit(self._patterns.getRule())

    def __toggleShowPatterns(self, position):
        self._patternsGroup.setVisible(True if position == 1 else False)
        self._random.setEnabled(True if position == 1 else False)

    def __setGroupTitle(self, rule):
        self._patternsGroup.setTitle("Rule %d" % (rule))


if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)
    ops = AutomataOptions()
    ops.show()

    sys.exit(app.exec_())
