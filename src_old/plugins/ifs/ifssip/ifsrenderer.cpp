/* *******************************************************************
 * *
 ** Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
 **
 ** This file is part of Fatou
 **
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **
 ***********************************************************************/


#include <QtGui>
#include <QTime>

#include "ifsrenderer.h"

IfsRenderer::IfsRenderer()
    : QObject()
{
    m_iterations = 10000;
    m_maps = QList< QList<double> >();

    m_size = QSize();

    QTime t = QTime::currentTime();
    qsrand((uint)t.msec());
}

IfsRenderer::~IfsRenderer()
{
}

void IfsRenderer::render(QSize imageSize)
{
    qDebug() << "rendering";

    m_size = imageSize;
    simulate(false);
    simulate(true);
}

void IfsRenderer::simulate(bool draw)
{
    if(m_maps.isEmpty())
        return;

    QImage image = QImage(m_size, QImage::Format_RGB32);
    double p, psum;
    double x, y, x0;
    long long i;
    int j;

    image.fill(qRgb(0, 0, 0));

    if(draw)
        x = y = 0.0;
    else
    {
        x = m_maps[0][4];
        y = m_maps[0][5];
        m_xmin = m_xmax = x;
        m_ymin = m_ymax = y;
    }

    m_abort = false;
    for(i = 0; i < (int)m_iterations; ++i)
    {
        if(m_abort)
            return;

        p = qrand() % 930 / 929.0; // random number between 0.0 and 1.0
        psum = 0.0;
        for(j = 0; j < m_maps.size(); ++j)
        {
            psum += m_maps[j][6];
            if(p <= psum)
                break;
        }
        x0 = x * m_maps[j][0] + y * m_maps[j][1] + m_maps[j][4];
        y = x * m_maps[j][2] + y * m_maps[j][3] + m_maps[j][5];
        x = x0;

        if(draw)
        {
            double rx, ry;
            rx = (int)((x - m_xmin) / (m_xmax - m_xmin) * (m_size.width() - 1));
            ry = m_size.height() - 1 - (int)((y - m_ymin) / (m_ymax - m_ymin) * (m_size.height() - 1));
            image.setPixel(rx, ry, qRgb(255, 255, 255));
        }
        else
        {
            m_xmin = qMin(x, m_xmin);
            m_xmax = qMax(x, m_xmax);
            m_ymin = qMin(y, m_ymin);
            m_ymax = qMax(y, m_ymax);
        }
    }

    if(draw)
        emit completed(image);
    else
        m_size.setHeight((int)(m_size.height() * (m_ymax - m_ymin) / (m_xmax - m_xmin)));
}

void IfsRenderer::setIterations(int iterations)
{
    m_iterations = iterations;
}

void IfsRenderer::clearMaps()
{
    m_maps.clear();
}

void IfsRenderer::appendToMaps(QList<double> ifsmap)
{
    m_maps << ifsmap;
}

void IfsRenderer::abort()
{
    m_abort = true;
}