/* *******************************************************************
 * *
 ** Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
 **
 ** This file is part of Fatou
 **
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **
 ***********************************************************************/


#ifndef IFSRENDERER_H
#define IFSRENDERER_H

#include <QImage>
#include <QDebug>

class IfsRenderer: public QObject
{
    Q_OBJECT
public:
    IfsRenderer();
    ~IfsRenderer();

    void render(QSize imageSize);

    void setIterations(int iterations);
    void clearMaps();
    void appendToMaps(QList<double> ifsmap);

    void abort();
signals:
    void completed(const QImage &image);

private:
    void simulate(bool draw);

    long long m_iterations;
    QSize m_size;
    bool m_abort;
    double m_xmin, m_xmax, m_ymin, m_ymax;

    QList< QList<double> > m_maps;
};

#endif
