#!/bin/sh

#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


case $1 in
    make)
        cd ifssip/

        echo "Compiling Qt Library..."
        qmake
        make

        echo "Generating SIP API Bindings..."
        sip -t Qt_4_7_1 -I /usr/share/sip/ -t WS_X11 -c . *sip

        echo "Compiling Python Module..."
        python2 configure.py
        make
        mv IfsRenderer.so ../IfsRenderer.so
        mv lib*so* ../
        ;;
    clean)
        cd ifssip/
        rm sip* Makefile *.sbf *.o
        rm -r .obj .moc
        cd ..
        rm IfsRenderer.so lib*so*
        ;;
    *)
        echo "Usage: ./setup.sh [make|clean]"
esac