#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

NAME = "Iterated Function Systems"
FRACTALS = [
    "Barnsley's Fern",
    "Dragon",
    "Heighway's Dragon",
    "Levy C Curve",
    "Sierpinski Triangle",
    "Pentadentrite",
]

FRACTAL_PRESET_OPTIONS = [
    [
        [0.0, 0.0, 0.0, 0.16, 0.0, 0.0, 0.01],
        [0.85, 0.04, -0.04, 0.85, 0.0, 1.6, 0.85],
        [0.2, -0.26, 0.23, 0.22, 0.0, 1.6, 0.07],
        [-0.15, 0.28, 0.26, 0.24, 0.0, 0.44, 0.07],
    ],
    [
        [0.824074, 0.281482, -0.212346, 0.864198, -1.882290, -0.110607, 0.787473],
        [0.088272, 0.520988, -0.463889, -0.377778, 0.785360, 8.095795, 0.212527],
    ],
    [[0.5, -0.5, 0.5, 0.5, 0.0, 0.0, 0.5], [-0.5, -0.5, 0.5, -0.5, 1.0, 0.0, 0.5]],
    [[0.5, -0.5, 0.5, 0.5, 0.0, 0.0, 0.5], [0.5, 0.5, -0.5, 0.5, 0.5, 0.5, 0.5]],
    [
        [0.5, 0.0, 0.0, 0.5, 0.0, 0.0, 0.33],
        [0.5, 0.0, 0.0, 0.5, 0.0, 1.0, 0.33],
        [0.5, 0.0, 0.0, 0.5, 1.0, 1.0, 0.34],
    ],
    [
        [0.341, -0.071, 0.071, 0.341, 0, 0, 0.167],
        [0.038, -0.346, 0.346, 0.038, 0.341, 0.071, 0.167],
        [0.341, -0.071, 0.071, 0.341, 0.379, 0.418, 0.166],
        [-0.234, 0.258, -0.258, -0.234, 0.72, 0.489, 0.167],
        [0.173, 0.302, -0.302, 0.173, 0.486, 0.231, 0.167],
        [0.341, -0.071, 0.071, 0.341, 0.659, -0.071, 0.166],
    ],
]
