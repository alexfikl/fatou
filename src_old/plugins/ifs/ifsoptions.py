#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


from PyQt5.QtCore import *
from PyQt5.QtGui import *

import plugins.abstractfractal as af

from .info import *


class IfsOptions(af.AbstractFractalOptions):
    iterationsChanged = pyqtSignal(int)
    mapChanged = pyqtSignal(list)
    start = pyqtSignal()

    def __init__(self, parent=None):
        af.AbstractFractalOptions.__init__(self, parent)

        self.__readSettings()

        self.__createWidgets()
        self.__createLayout()
        self.setFixedWidth(260)

    def __readSettings(self):
        settings = QSettings("fatou", "fatou")

        settings.beginGroup("ifs")
        self._current = settings.value("current", 0).toInt()[0]
        self._iterationsV = settings.value("iterations", 10000).toInt()[0]
        settings.endGroup()

    def saveSettings(self):
        settings = QSettings("fatou", "fatou")

        settings.beginGroup("ifs")
        settings.setValue("current", self._presets.currentIndex())
        settings.setValue("iterations", self._iterationsLimit.value())
        settings.endGroup()

    def __createWidgets(self):
        self._presets = QComboBox(self)
        for index, f in enumerate(FRACTALS):
            self._presets.addItem(f)
        self._presets.setCurrentIndex(self._current)

        self._iterationsLimit = QSpinBox(self)
        self._iterationsLimit.setRange(0, 1000000)
        self._iterationsLimit.setSingleStep(1000)
        self._iterationsLimit.setMinimumWidth(144)
        self._iterationsLimit.setValue(self._iterationsV)

    def __createLayout(self):
        optionsLayout = QVBoxLayout()

        group = QGroupBox("Options")
        layout = QFormLayout()
        layout.setLabelAlignment(Qt.AlignRight)
        layout.addRow("Presets:", self._presets)
        layout.addRow("Iterations:", self._iterationsLimit)
        group.setLayout(layout)
        optionsLayout.addWidget(group)
        optionsLayout.addStretch()

        self.setLayout(optionsLayout)

    def emitStartData(self):
        self.emitData()
        self.start.emit()

    def emitData(self):
        i = self._presets.currentIndex()
        self.iterationsChanged.emit(self._iterationsLimit.value())
        self.mapChanged.emit(FRACTAL_PRESET_OPTIONS[i])


if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)

    ops = IfsOptions()
    qApp.aboutToQuit.connect(ops.saveSettings)
    ops.show()

    sys.exit(app.exec_())
