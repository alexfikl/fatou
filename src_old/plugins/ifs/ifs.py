#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

from PyQt5.QtCore import *
from PyQt5.QtGui import *

from plugins.ifs.IfsRenderer import IfsRenderer


class IfsWidget(QWidget):
    information = pyqtSignal(QString)
    stopped = pyqtSignal()
    render = pyqtSignal(QSize)

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self._pixmap = QPixmap()

        self._thread = QThread()
        self._renderer = IfsRenderer()
        self._renderer.moveToThread(self._thread)
        self.render.connect(self._thread.start)
        self.render.connect(self._renderer.render)
        self._renderer.completed.connect(self.__updatePixmap)
        self._renderer.completed.connect(self._thread.quit)
        self._renderer.completed.connect(self.stopped)

        self.setMinimumSize(400, 400)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.fillRect(self.rect(), Qt.black)

        if self._pixmap.isNull():
            painter.setPen(Qt.white)
            painter.drawText(
                self.rect(), Qt.AlignCenter, "Rendering initial image, please wait."
            )
            return

        # center the image
        offset = QPointF(0, 0)
        if self._pixmap.width() < self.width():
            offset.setX((self.width() - self._pixmap.width()) / 2)
        if self._pixmap.height() < self.height():
            offset.setY((self.height() - self._pixmap.height()) / 2)

        painter.drawPixmap(offset, self._pixmap)

    def resizeEvent(self, event):
        self._renderer.abort()
        self.render.emit(self.size())

    def __updatePixmap(self, image):
        image = image.scaled(self.size(), Qt.KeepAspectRatio)
        self._pixmap = QPixmap.fromImage(image)
        self.update()

    def saveTo(self, filename):
        image = self._pixmap.toImage()
        image.save(filename)

    def start(self):
        self.render.emit(self.size())

    def stop(self):
        self._renderer.abort()

    def setIterations(self, its):
        self._renderer.setIterations(its)

    def setMap(self, newmaps):
        self._renderer.clearMaps()
        for m in newmaps:
            self._renderer.appendToMaps(m)


if __name__ == "__main__":
    import sys

    mapz = [
        [0.5, 0.0, 0.0, 0.5, 0.0, 0.0, 0.33],
        [0.5, 0.0, 0.0, 0.5, 0.0, 1.0, 0.33],
        [0.5, 0.0, 0.0, 0.5, 1.0, 1.0, 0.34],
    ]
    app = QApplication(sys.argv)
    exp = IfsWidget()
    exp.setMap(mapz)
    exp.setIterations(100000)
    exp.resize(400, 300)
    exp.show()

    sys.exit(app.exec_())
