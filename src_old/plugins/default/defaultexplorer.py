#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


from PyQt5.QtCore import Qt
from PyQt5.QtGui import *

from plugins.abstractfractal import AbstractFractalExplorer


class DefaultExplorer(AbstractFractalExplorer):
    def __init__(self, parent=None):
        AbstractFractalExplorer.__init__(self, parent)

    def paintEvent(self, ev):
        painter = QPainter(self)
        font = QFont("DejaVu", 20)

        painter.fillRect(self.rect(), Qt.black)
        painter.setPen(Qt.white)
        painter.setFont(font)
        painter.drawText(self.rect(), Qt.AlignCenter, "Welcome to Fatou!")


if __name__ == "__main__":
    import sys

    app = QApplication(sys.argv)
    ex = DefaultExplorer()
    ex.resize(400, 400)
    ex.show()

    sys.exit(app.exec_())
