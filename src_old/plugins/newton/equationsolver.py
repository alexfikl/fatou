#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


import math
import re

import numpy


class EquationSolver:
    def __init__(self):
        self._eq = ""
        self._coeffs = []
        self._roots = []

    def setEquation(self, eq):
        if not eq:
            return

        eq = str(eq)
        variable = eq.translate(None, "1234567890.+-*/^ ")

        if variable:
            variable = variable[0]
        else:
            return

        self._coeffs = []
        self._roots = []
        self._eq = eq
        self._powerRegex = re.compile(r"%s\^?(\d)?" % (variable))
        self._termRegex = re.compile(
            r"([+-]?\s*\d*\.?\d*)\s*\*?\s*(%s?\^?\d?)" % (variable)
        )
        self.__getCoefficients()

    def equation(self):
        return self._eq

    def roots(self):
        if self._coeffs:
            if not self._roots:
                self._roots = [complex(r) for r in numpy.roots(self._coeffs[::-1])]
            return [(r.real, r.imag) for r in self._roots]
        else:
            return []

    def coefficients(self):
        if not self._coeffs:
            self.__getCoefficients()
        return self._coeffs

    def __evalCoefficient(self, s):
        if not s:
            return 1
        if s == "-":
            return -1
        else:
            try:
                return float(s)
            except ValueError:
                return 0

    def __evalPower(self, s):
        if not s:
            return 0
        res = self._powerRegex.findall(s)[0]
        return int(res) if res else 1

    def __getCoefficients(self):
        plist = []
        for coeff, term in self._termRegex.findall(self._eq):
            coeff = "".join([i for i in coeff if i != " "])
            term = "".join([i for i in term if i != " "])
            if not coeff and not term:
                continue
            coeff = self.__evalCoefficient(coeff)
            term = self.__evalPower(term)
            plist.append((term, coeff))

        size = max(plist, key=lambda x: x[0])[0] + 1
        self._coeffs = [0] * size
        for p, c in plist:
            self._coeffs[p] = c


if __name__ == "__main__":
    eq = EquationSolver()
    eq.setEquation("x^3 - 2x + 2")
    print(eq.roots())
