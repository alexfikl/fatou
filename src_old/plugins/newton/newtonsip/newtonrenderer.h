/* *******************************************************************
 * *
 ** Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
 **
 ** This file is part of Fatou
 **
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **
 ***********************************************************************/


#ifndef NEWTONRENDERER_H
#define NEWTONRENDERER_H

#include <QImage>
#include <QGradientStops>
#include <QDebug>

class NewtonRenderer: public QObject
{
    Q_OBJECT
public:
    NewtonRenderer();
    ~NewtonRenderer();

    void render(QSize imageSize, int type);
    void color(QSize size, int type);

    void setIterations(int iterations);

    void setCenterX(double x);
    void setCenterY(double y);
    void setCenterCoordinates(double x, double y);
    void setScale(double scale);

    void setPolynomial(QList<double> coeff);
    void setRoots(QList< QPair<double, double> > roots);

    void setGradientStops(QGradientStops stops);
    void setColorMapSize(int size);
    void setColorDensity(int density);
    void setColorByRoot(bool color);

    void abort();

    double centerX();
    double centerY();
    double scale();
signals:
    void completed(const QImage &image, double scaleFactor);

private:
    uint getColorFor(int i, int type);
    QPair<double, double> numericalDerivative(QPair<double, double> z, int type);

    void iterate(double x, double y, int type);

    // two already defined function and a generic one that operates on coeffs
    QPair<double, double> f(QPair<double, double> z, int type);
    QPair<double, double> function1(QPair<double, double> z); /* z^3 - 1 */
    QPair<double, double> function2(QPair<double, double> z); /* z^4 - 1 */
    QPair<double, double> function3(QPair<double, double> z); /* z^3 - 2x + 2 */
    QPair<double, double> customFunction(QPair<double, double> z);

    // functions for operations on complex numbers
    // TODO: make a complex numbers class
    QPair<double, double> add(QPair<double, double> x, QPair<double, double> y);
    QPair<double, double> diff(QPair<double, double> x, QPair<double, double> y);
    QPair<double, double> mul(QPair<double, double> x, QPair<double, double> y);
    QPair<double, double> div(QPair<double, double> x, QPair<double, double> y);
    double cabs(QPair<double, double> x);

    double m_centerX;
    double m_centerY;

    bool m_abort;
    bool m_colorRoots;

    double m_scaleFactor;
    uint m_iterations;

    QList<int> m_iterationsList;
    QList<int> m_rootList;
    QList<uint> m_colormap;

    QList<double> m_coefficients;
    QList< QPair<double, double> > m_roots;

    uint m_colorMapSize;
    uint m_colorDensity;
};

#endif
