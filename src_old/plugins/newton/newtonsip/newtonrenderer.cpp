/* *******************************************************************
 * *
 ** Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
 **
 ** This file is part of Fatou
 **
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **
 ***********************************************************************/


#include <QtGui>

#include <math.h>

#include "newtonrenderer.h"

const double epsilon = 1e-12;

NewtonRenderer::NewtonRenderer()
    : QObject()
{
    m_scaleFactor = 0.0078125;
    m_centerX = -0.637011;
    m_centerY = 0.0;
    m_colorRoots = false;

    m_iterations = 50;
    m_colorMapSize = 512;
}

NewtonRenderer::~NewtonRenderer()
{
}

void NewtonRenderer::render(QSize size, int type)
{
    qDebug() << "rendering";
    qDebug() << "center" << m_centerX << m_centerY;

    int halfWidth = size.width() / 2;
    int halfHeight = size.height() / 2;

    m_abort = false;
    m_iterationsList.clear();
    m_rootList.clear();

    for(int y = -halfHeight; y < halfHeight; ++y)
    {
        if(m_abort)
            return;

        double ay = m_centerY + (y * m_scaleFactor);
        for(int x = -halfWidth; x < halfWidth; ++x)
        {
            double ax = m_centerX + (x * m_scaleFactor);
            iterate(ax, ay, type);
        }
    }

    color(size, type);
}

void NewtonRenderer::color(QSize size, int type)
{
    qDebug() << "coloring";
    if(m_iterationsList.isEmpty())
        return;

    int halfWidth = size.width() / 2;
    int halfHeight = size.height() / 2;
    QImage image(size, QImage::Format_RGB32);
    int i = 0;

    for(int y = -halfHeight; y < halfHeight; ++y)
    {
        uint *scanLine = reinterpret_cast<uint *>(image.scanLine(y + halfHeight));
        for(int x = -halfWidth; x < halfWidth; ++x)
        {
            *scanLine++ = getColorFor(i, type);
            ++i;
        }
    }

    emit completed(image, m_scaleFactor);
}

void NewtonRenderer::iterate(double x, double y, int type)
{
    uint i;
    QPair<double, double> dz, z, z0;

    z.first = x;
    z.second = y;

    for(i = 0; i < m_iterations; ++i)
    {
        dz = numericalDerivative(z, type);
        z0 = diff(z, div(f(z, type), dz));
        if(cabs(diff(z, z0)) < epsilon)
            break;
        z = z0;
    }

    m_iterationsList << i;
}

uint NewtonRenderer::getColorFor(int i, int type)
{
    Q_UNUSED(type);

    if(m_iterationsList.at(i) <= 0)
        return qRgb(0, 0, 0);
    else
    {
        return m_colormap.at(m_colorDensity * m_iterationsList[i] % m_colorMapSize);
    }
}

QPair<double, double> NewtonRenderer::f(QPair<double, double> z, int type)
{
    switch(type)
    {
        case 0:
            return customFunction(z);
        case 1:
            return function1(z);
        case 2:
            return function2(z);
        case 3:
            return function3(z);
    }
    return QPair<double, double>(1.0, 0.0);
}

QPair<double, double> NewtonRenderer::numericalDerivative(QPair<double, double> z, int type)
{
    double temp;
    QPair<double, double> h;

    temp = sqrt(2.2e-16);
    h.first = temp * z.first;
    h.second = temp * z.second;

    // we have h and want complex(h, h)
    temp = h.first;
    h.first = h.first - h.second;
    h.second = temp + h.second;

    // compute (f(z + h) - f(z)) / h
    z = div(diff(f(add(z, h), type), f(z, type)), h);

    if(z.first == 0 and z.second == 0)
    {
        z.first = 1;
        z.second = 0;
    }

    return z;
}

void NewtonRenderer::setIterations(int iterations)
{
    m_iterations = iterations;
}

QPair<double, double> NewtonRenderer::customFunction(QPair<double, double> z)
{
    QPair<double, double> result(0.0, 0.0);
    for(int i = 0; i < m_coefficients.size(); ++i)
    {
        if(m_coefficients.at(i) != 0.0)
        {
            QPair<double, double> power(1.0, 0.0);
            for(int j = 0; j < i; ++j)
                power = mul(power, z);

            result = add(result, mul(QPair<double, double>(m_coefficients.at(i), 0.0), power));
        }
    }

    return result;
}

QPair<double, double> NewtonRenderer::function1(QPair<double, double> z)
{
    // z^3 - 1
    double x, x0, y;
    x = z.first;
    y = z.second;
    x0 = x * x * x - 3.0 * x * y * y - 1;
    y = 3.0 * x * x * y - y * y * y;
    return QPair<double, double>(x0, y);
}

QPair<double, double> NewtonRenderer::function2(QPair<double, double> z)
{
    // z^4 - 1
    double x, x0, y;
    x = z.first;
    y = z.second;
    x0 = x * x * x * x - 6.0 * x * x * y * y + y * y * y * y - 1;
    y = 4.0 * x * x * x * y - 4.0 * x * y * y * y;
    return QPair<double, double>(x0, y);
}

QPair<double, double> NewtonRenderer::function3(QPair<double, double> z)
{
    // z^3 - 2z + 2
    double x, x0, y;
    x = z.first;
    y = z.second;
    x0 = x * x * x - 3.0 * x * y * y - 2.0 * x + 2;
    y = 3.0 * x * x * y - y * y * y - 2.0 * y;
    return QPair<double, double>(x0, y);
}

QPair<double, double> NewtonRenderer::add(QPair<double, double> x, QPair<double, double> y)
{
    x.first += y.first;
    x.second += y.second;
    return x;
}

QPair<double, double> NewtonRenderer::mul(QPair<double, double> x, QPair<double, double> y)
{
    double x0, y0;
    x0 = x.first * y.first - x.second * y.second;
    y0 = x.second * y.first + x.first * y.second;
    return QPair<double, double>(x0, y0);
}

QPair<double, double> NewtonRenderer::diff(QPair<double, double> x, QPair<double, double> y)
{
    x.first -= y.first;
    x.second -= y.second;
    return x;
}

QPair<double, double> NewtonRenderer::div(QPair<double, double> x, QPair<double, double> y)
{
    double x0, y0;
    x0 = (x.first * y.first + x.second * y.second);
    x0 = x0 / (y.first * y.first + y.second * y.second);

    y0 = (x.second * y.first - x.first * y.second);
    y0 = y0 / (y.first * y.first + y.second * y.second);

    return QPair<double, double>(x0, y0);
}

double NewtonRenderer::cabs(QPair<double, double> x)
{
    return sqrt(x.first * x.first + x.second * x.second);
}

void NewtonRenderer::setCenterX(double x)
{
    m_centerX = x;
}

void NewtonRenderer::setCenterY(double y)
{
    m_centerY = y;
}

void NewtonRenderer::setColorMapSize(int size)
{
    m_colorMapSize = size;
}

void NewtonRenderer::setColorDensity(int density)
{
    m_colorDensity = density;
}

void NewtonRenderer::setGradientStops(QGradientStops stops)
{
    // Gradient generation
    QImage gradientImage(m_colorMapSize, 1, QImage::Format_ARGB32);
    QPainter painter(&gradientImage);

    QLinearGradient gradient(0, 0, m_colorMapSize, 0);
    gradient.setStops(stops);

    painter.fillRect(gradientImage.rect(), gradient);

    // Colormap filling
    if(m_colorMapSize != (uint)m_colormap.size())
        m_colormap.clear();

    for(uint i = 0; i < m_colorMapSize; ++i)
        m_colormap << gradientImage.pixel(i, 0);
}

void NewtonRenderer::setScale(double scale)
{
    m_scaleFactor = scale;
}

void NewtonRenderer::setCenterCoordinates(double x, double y)
{
    setCenterX(x);
    setCenterY(y);
}

void NewtonRenderer::setPolynomial(QList<double> coeff)
{
    m_coefficients = coeff;
}

void NewtonRenderer::setRoots(QList< QPair<double, double> > roots)
{
    m_roots = roots;
}

void NewtonRenderer::setColorByRoot(bool color)
{
    m_colorRoots = color;
}

void NewtonRenderer::abort()
{
    m_abort = true;
}

double NewtonRenderer::centerX()
{
    return m_centerX;
}

double NewtonRenderer::centerY()
{
    return m_centerY;
}

double NewtonRenderer::scale()
{
    return m_scaleFactor;
}