"""Module providing an easy to use color picker built upon gradients.
"""


#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

import PyQt5.QtGui as qg
from PyQt5.QtCore import QLineF, QPointF, Qt, pyqtSignal

from .hoverpoints import HoverPoints


class ShadeWidget(qg.QWidget):
    """Class providing a certain color for the editor.
    Available colors are Red, Green, Blue.
    """

    colorsChanged = pyqtSignal(qg.QPolygonF)

    RedShade = 1
    GreenShade = 2
    BlueShade = 3

    def __init__(self, shadeType, parent=None):
        qg.QWidget.__init__(self, parent)

        self._shadeType = shadeType
        self._shade = qg.QImage()

        self.setAttribute(Qt.WA_OpaquePaintEvent)

        points = qg.QPolygonF()
        points.append(QPointF(0, 0))
        points.append(QPointF(self.size().width(), 0))

        self._hoverPoints = HoverPoints(self)
        self._hoverPoints.setPoints(points)

        self.setSizePolicy(qg.QSizePolicy.Preferred, qg.QSizePolicy.Preferred)
        self._hoverPoints.pointsChanged.connect(self.colorsChanged)

    def points(self):
        """Returns the hover points currently on the widget.
        The points are part of a QPolygonF.
        """
        return self._hoverPoints.points()

    def setHoverPoints(self, points):
        """Sets new hover points. The points must be a QPolygonF."""
        self._hoverPoints.setPoints(points)

    def colorAt(self, x):
        """Returns the color at a certain width.
        Iterates through all the available hover points until it finds the
        one closest to the given point.
        """
        self.__generateShade()

        pts = self._hoverPoints.points()

        for i in range(len(pts)):
            if pts[i - 1].x() < x and pts[i].x() >= x:
                line = QLineF(pts[i - 1], pts[i])
                line.setLength((line.length() * (x - line.x1())) / line.dx())

                px = int(min(line.x2(), self._shade.width() - 1))
                py = int(min(line.y2(), self._shade.height() - 1))
                return self._shade.pixel(px, py)

        return 0

    def paintEvent(self, _):
        """Implemented virtual paintEvent function.
        Generates the gradient and then paints it on the image.
        """
        self.__generateShade()

        painter = qg.QPainter(self)
        painter.drawImage(0, 0, self._shade)
        painter.setPen(qg.QColor(146, 146, 146))
        painter.drawRect(0, 0, self.width() - 1, self.height() - 1)

    def __generateShade(self):
        if self._shade.isNull() or self._shade.size() != self.size():
            self._shade = qg.QImage(self.size(), qg.QImage.Format_RGB32)
            shade = qg.QLinearGradient(0, 0, 0, self.height())
            shade.setColorAt(1, Qt.black)

            if self._shadeType == self.RedShade:
                shade.setColorAt(0, Qt.red)
            elif self._shadeType == self.GreenShade:
                shade.setColorAt(0, Qt.green)
            else:
                shade.setColorAt(0, Qt.blue)

            painter = qg.QPainter(self._shade)
            painter.fillRect(self.rect(), shade)


class GradientEditor(qg.QWidget):
    """A color picker class in the form of a gradient editor.
    Composed of three gradient widgets (ShadeWidget) one for
    each available color.
    """

    gradientStopsChanged = pyqtSignal(list)

    def __init__(self, parent=None):
        qg.QWidget.__init__(self, parent)

        vbox = qg.QVBoxLayout(self)
        vbox.setSpacing(1)
        vbox.setMargin(1)

        self._redShade = ShadeWidget(ShadeWidget.RedShade, self)
        self._greenShade = ShadeWidget(ShadeWidget.GreenShade, self)
        self._blueShade = ShadeWidget(ShadeWidget.BlueShade, self)

        vbox.addWidget(self._redShade)
        vbox.addWidget(self._greenShade)
        vbox.addWidget(self._blueShade)

        self._redShade.colorsChanged.connect(self.__pointsUpdated)
        self._greenShade.colorsChanged.connect(self.__pointsUpdated)
        self._blueShade.colorsChanged.connect(self.__pointsUpdated)

        self.setMinimumSize(300, 150)

    def setGradientStops(self, newStops):
        """Used to transform from QGradientStop to QPolygonF for each color."""
        pts = {i: qg.QPolygonF() for i in ["red", "gr", "blue"]}

        for sp in newStops:
            red, green, blue = sp[1].getRgb()[:3]

            redw, redh = self._redShade.width(), self._redShade.height()
            grw, grh = self._greenShade.width(), self._greenShade.height()
            blw, blh = self._blueShade.width(), self._blueShade.height()

            pts["red"].append(QPointF(sp[0] * redw, redh - red * redh / 255.0))
            pts["gr"].append(QPointF(sp[0] * grw, grh - green * grh / 255.0))
            pts["blue"].append(QPointF(sp[0] * blw, blh - blue * blh / 255.0))

        setShadePoints(pts["red"], self._redShade)
        setShadePoints(pts["gr"], self._greenShade)
        setShadePoints(pts["blue"], self._blueShade)

    def __pointsUpdated(self):
        w = float(self._redShade.width())

        points = qg.QPolygonF()

        stops = []
        points += self._redShade.points()
        points += self._greenShade.points()
        points += self._blueShade.points()

        points = qg.QPolygonF(sorted(points, key=lambda p: p.x()))

        for i in range(len(points)):
            x = int(points.at(i).x())
            if i < (len(points) - 1) and x == points.at(i + 1).x():
                continue

            r = (0x00FF0000 & self._redShade.colorAt(int(x))) >> 16
            g = (0x0000FF00 & self._greenShade.colorAt(int(x))) >> 8
            b = 0x000000FF & self._blueShade.colorAt(int(x))
            color = qg.QColor(r, g, b)
            x = round(x / w, 2)
            if x > 1:
                return 0

            if not any(abs(x - r) < 0.1e-3 for r, color in stops):
                stops.append((x, color))

        self.gradientStopsChanged.emit(stops)


def setShadePoints(points, shadeWidget):
    """Sets new hover points for a specific ShadeWidget."""
    shadeWidget.setHoverPoints(points)
    shadeWidget.update()


if __name__ == "__main__":
    import sys

    APP = qg.QApplication(sys.argv)
    EDITOR = GradientEditor()
    EDITOR.resize(500, 300)
    EDITOR.show()

    STOPS = []
    STOPS.append((0.00, qg.QColor(255, 255, 255)))
    STOPS.append((0.10, qg.QColor.fromRgba(0xFFE0CC73)))
    STOPS.append((0.17, qg.QColor.fromRgba(0xFFC6A006)))
    STOPS.append((0.46, qg.QColor.fromRgba(0xFF600659)))
    STOPS.append((0.72, qg.QColor.fromRgba(0xFF0680AC)))
    STOPS.append((0.92, qg.QColor.fromRgba(0xFFB9D9E6)))
    STOPS.append((1.00, qg.QColor(255, 255, 255)))

    EDITOR.setGradientStops(STOPS)

    sys.exit(APP.exec_())
