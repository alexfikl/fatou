"""Module that provides a class for drawing hover points on top of the
ShadeWidget.
"""


#############################################################################
## Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
##
## This file is part of Fatou
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


import PyQt5.QtGui as qg
from PyQt5.QtCore import (QEvent, QObject, QPointF, QRectF, QSize, Qt,
                          pyqtSignal)


class HoverPoints(QObject):
    """Class that provides the representation of a list of hover points on
    top of a ShadeWidget.
    """

    pointsChanged = pyqtSignal(qg.QPolygonF)

    NoLock = 0
    LockToLeft = 1
    LockToRight = 2

    def __init__(self, widget):
        QObject.__init__(self, widget)

        self._widget = widget
        self._widget.installEventFilter(self)
        self._pointSize = QSize(11, 11)
        self._hoverPoints = qg.QPolygonF
        self._currentIndex = -1

    def points(self):
        """Returns current hover points."""
        return self._hoverPoints

    def setPoints(self, pts):
        """Sets new hover points. The left-most and right-most points are
        locked on their respective side.
        """
        hoverPoints = [self.__boundPoint(pts[0], HoverPoints.LockToLeft)]
        hoverPoints.extend([self.__boundPoint(i, 0) for i in pts[1:-1]])
        hoverPoints.append(self.__boundPoint(pts[-1], HoverPoints.LockToRight))

        self._hoverPoints = qg.QPolygonF(hoverPoints)

    def eventFilter(self, obj, ev):
        """Implementation of the virtual function. Handles all events sent to
        the object.
        """
        if obj == self._widget:
            switch = {
                QEvent.MouseButtonPress: self.__handleMouseButtonPress,
                QEvent.MouseButtonRelease: self.__handleMouseButtonRelease,
                QEvent.MouseMove: self.__handleMouseMove,
                QEvent.Resize: self.__handleResize,
                QEvent.Paint: self.__handlePaint,
            }

            return switch.get(ev.type(), lambda ev: False)(ev)

        return False

    def __pointBoundingRect(self, point):
        w = self._pointSize.width()
        h = self._pointSize.height()

        return QRectF(point.x() - w / 2.0, point.y() - h / 2.0, w, h)

    def __handleMouseButtonPress(self, ev):
        clickPos = QPointF(ev.pos())
        index = -1
        i = 0

        for i, point in enumerate(self._hoverPoints):
            path = qg.QPainterPath()
            path.addEllipse(self.__pointBoundingRect(point))

            if path.contains(clickPos):
                index = i
                break

        if ev.button() == Qt.LeftButton:
            if index == -1:
                pos = 0

                for point in self._hoverPoints:
                    if point.x() > clickPos.x():
                        pos = i
                        break

                self._hoverPoints.insert(pos, clickPos)
                self._currentIndex = pos
                self.__firePointChange(True)
            else:
                self._currentIndex = index
            return True
        elif ev.button() == Qt.RightButton:
            if index > 0 and index < (len(self._hoverPoints) - 1):
                self._hoverPoints.remove(index)
                self.__firePointChange(True)
                return True

        return False

    def __handleMouseButtonRelease(self, _):
        self._currentIndex = -1
        self.__firePointChange(True)

        return False

    def __handleMouseMove(self, ev):
        if self._currentIndex >= 0:
            self.__movePoint(self._currentIndex, ev.pos(), False)

        return False

    def __handleResize(self, ev):
        if not ev.oldSize().isEmpty():
            stretchX = float(ev.size().width()) / ev.oldSize().width()
            stretchY = float(ev.size().height()) / ev.oldSize().height()

            for i, pt in enumerate(self._hoverPoints):
                np = QPointF(pt.x() * stretchX, pt.y() * stretchY)
                self.__movePoint(i, np, False)

            self.__firePointChange(True)

        return False

    def __handlePaint(self, ev):
        currentWidget = self._widget
        self._widget = None
        qg.QApplication.sendEvent(currentWidget, ev)
        self._widget = currentWidget
        self.__paintPoints()

        return True

    def __paintPoints(self):
        painter = qg.QPainter()
        painter.begin(self._widget)
        painter.setRenderHint(qg.QPainter.Antialiasing)

        painter.setPen(qg.QPen(qg.QColor(255, 255, 255, 127), 2))

        path = qg.QPainterPath()
        path.moveTo(self._hoverPoints[0])

        for i in range(1, len(self._hoverPoints)):
            p1 = self._hoverPoints[i - 1]
            p2 = self._hoverPoints[i]
            distance = p2.x() - p1.x()

            path.cubicTo(
                p1.x() + distance / 2,
                p1.y(),
                p1.x() + distance / 2,
                p2.y(),
                p2.x(),
                p2.y(),
            )

        painter.drawPath(path)

        painter.setPen(qg.QPen(qg.QColor(255, 255, 255, 191), 1))
        painter.setBrush(qg.QBrush(qg.QColor(191, 191, 191, 127)))

        for point in self._hoverPoints:
            painter.drawEllipse(self.__pointBoundingRect(point))

    def __boundPoint(self, point, lock):
        left = self._widget.rect().left()
        right = self._widget.rect().right()
        top = self._widget.rect().top()
        bottom = self._widget.rect().bottom()

        if point.x() < left or lock == HoverPoints.LockToLeft:
            point.setX(left)
        elif point.x() > right or lock == HoverPoints.LockToRight:
            point.setX(right)

        if point.y() < top:
            point.setY(top)
        elif point.y() > bottom:
            point.setY(bottom)

        return QPointF(point)

    def __movePoint(self, index, point, emitUpdate):
        if index == 0:
            lock = HoverPoints.LockToLeft
        elif index == (len(self._hoverPoints) - 1):
            lock = HoverPoints.LockToRight
        else:
            lock = HoverPoints.NoLock

        self._hoverPoints[index] = self.__boundPoint(point, lock)

        self.__firePointChange(emitUpdate)

    def __firePointChange(self, emitUpdate):
        oldCurrent = QPointF()
        if self._currentIndex != -1:
            oldCurrent = self._hoverPoints[self._currentIndex]

        self._hoverPoints = sorted(self._hoverPoints, key=lambda p: p.x())
        self._hoverPoints = qg.QPolygonF(self._hoverPoints)

        if self._currentIndex != -1:
            for index, point in enumerate(self._hoverPoints):
                if point == oldCurrent:
                    self._currentIndex = index
                    break

        self._widget.update()
        if emitUpdate:
            self.pointsChanged.emit(self._hoverPoints)
