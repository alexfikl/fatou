# SPDX-FileCopyrightText: 2011,2022 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import sys
import zipfile

from PyQt5.QtCore import QObject, pyqtSignal


class PluginManager(QObject):
    """Plugin Manager that takes care of anything plugin related.

    For installing plugins the following folder structure is required:
    - pluginname/
        - pluginnameexplorer.py
        - other files etc.

    The pluginnameexplorer.py file need to contain a class named
    PluginnameExplorer, with only the first letter uppercased.

    Plugins are provided in the form of zip files.
    """

    pluginListChanged = pyqtSignal(list, int)

    def __init__(self, folder="plugins"):
        super().__init__()
        self._plugins = {}
        self._pluginFolder = folder

        self.__loadPlugins()

    def getPlugin(self, plugin_id):
        """Returns the class for a given plugin_id, if it exists."""
        plugin_id = str(plugin_id)
        try:
            plugin_id = plugin_id.lower()
            imp = self._plugins[plugin_id]["import"]
            name = self._plugins[plugin_id]["name"]
            cls = self._plugins[plugin_id]["class"]

            __import__(imp)
            imp = sys.modules[imp]
            return (name, getattr(imp, cls, None))
        except AttributeError as e:
            print(e)
            return ("", None)
        except ImportError:
            print("import")
            return ("", None)

    def refresh(self):
        """Reloads the plugin list."""
        self.__loadPlugins()

    def installPlugin(self, zip_filename):
        ids, error = self._extract(str(zip_filename))
        self.pluginListChanged.emit(ids, error)

        if not error:
            self.__addPlugin(ids)

    def listPlugins(self):
        """Returns a list of plugin information containing:
        - the plugin's id
        - the plugin's name
        - the plugin's list of provided functionality
        """
        plugins_info = []
        for pid, plugin in list(self._plugins.items()):
            plugins_info.append((pid, plugin["name"], plugin["fractals"]))

        return plugins_info

    def __loadPlugins(self):
        plugins_id = []
        # check all files in the directory
        for item in os.listdir(self._pluginFolder):
            # only add folders
            if os.path.isdir(os.path.join(self._pluginFolder, item)):
                plugins_id.append(item.lower())
        # remove default plugin which is just a welcome screen
        if "default" in plugins_id:
            plugins_id.remove("default")
        if "__pycache__" in plugins_id:
            plugins_id.remove("__pycache__")

        for pid in plugins_id:
            self.__addPlugin(pid)

    def __extract(self, source):
        # get zip file
        try:
            zfile = zipfile.ZipFile(source, "r")
        except zipfile.BadZipfile:
            return ("", 1)

        # check data
        flist = zfile.namelist()
        # get plugin name: find a base folder
        pid = [x for x in flist if x.count("/") == 1 and x.endswith("/")][0]

        # get expected explorer file path and check if it's in the file
        explorer = f"{pid}{pid[:-1]}explorer.py"
        if explorer not in flist:
            return (pid[:-1], 2)

        # check to see if the explorer class is in the explorer file
        exp_class = f"{pid[:-1].capitalize()}Explorer"
        has_class = False
        for line in zfile.read(explorer).split("\n"):
            if exp_class in line:
                has_class = True
                break

        if not has_class:
            return (pid[:-1], 3)

        # unzip: if directory makedir else write_data
        for filename in flist:
            filepath = self._pluginFolder + filename
            try:
                if filename.endswith("/"):
                    os.makedirs(filepath)
                else:
                    data = zfile.read(filename)
                    with open(filepath, "w") as fd:
                        fd.write(data)
            except OSError:
                return ("", 4)

        return (pid[:-1], 0)

    def __addPlugin(self, pid):
        plugin = {}
        basename = os.path.basename(self._pluginFolder.rstrip("/"))
        import_name = "{}.{}.{}explorer".format(basename, pid, pid)
        info_import = "{}.{}.info".format(basename, pid)

        __import__(import_name)
        __import__(info_import)

        pidc = pid.capitalize()
        plugin["class"] = "%sExplorer" % (pidc)
        plugin["import"] = import_name
        plugin["name"] = getattr(sys.modules[info_import], "NAME", pidc)
        plugin["fractals"] = getattr(sys.modules[info_import], "FRACTALS", "")

        self._plugins[pid] = plugin


if __name__ == "__main__":
    PM = PluginManager()
    print(PM.listPlugins())
