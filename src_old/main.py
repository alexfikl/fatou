#!/usr/bin/env python

# SPDX-FileCopyrightText: 2011,2022 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import sys

from PyQt5.QtWidgets import QApplication

import actionmanager
import fatou
import pluginmanager

if __name__ == "__main__":
    app = QApplication(sys.argv)

    pm = pluginmanager.PluginManager()
    am = actionmanager.ActionManager()
    window = fatou.FatouWindow(am, pm)
    window.setWindowTitle("Fatou")
    window.show()

    sys.exit(app.exec_())
