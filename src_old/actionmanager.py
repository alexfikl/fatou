# SPDX-FileCopyrightText: 2011,2022 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt5.QtCore import QObject
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtWidgets import QAction


class ActionManager(QObject):
    def __init__(self):
        QObject.__init__(self)

        self._actions = {
            "save": None,
            "start": None,
            "stop": None,
            "select": None,
            "zoom-in": None,
            "zoom-out": None,
        }

        self._createActions()

    def _createActions(self):
        self._actions["save"] = QAction("&Save", self)
        self._actions["save"].setShortcut(QKeySequence.SaveAs)
        self._actions["save"].setIcon(QIcon.fromTheme("document-save-as"))
        self._actions["save"].setIconText("Save")
        self._actions["save"].setToolTip("Save Fractal As")

        self._actions["start"] = QAction("S&tart", self)
        self._actions["start"].setShortcut(QKeySequence.Refresh)
        self._actions["start"].setIcon(QIcon.fromTheme("media-playback-start"))
        self._actions["start"].setToolTip("Start Fractal Rendering")

        self._actions["stop"] = QAction("St&op", self)
        self._actions["stop"].setShortcut(QKeySequence.Close)
        self._actions["stop"].setIcon(QIcon.fromTheme("media-playback-stop"))
        self._actions["stop"].setToolTip("Stop Fractal Rendering")

        self._actions["select"] = QAction("Sele&ct", self)
        self._actions["select"].setShortcut(QKeySequence.Open)
        self._actions["select"].setIcon(QIcon.fromTheme("view-media-playlist"))
        self._actions["select"].setToolTip("Select a Fractal Type")

        self._actions["zoom-in"] = QAction("&Zoom In", self)
        self._actions["zoom-in"].setShortcut(QKeySequence.ZoomIn)
        self._actions["zoom-in"].setIcon(QIcon.fromTheme("zoom-in"))

        self._actions["zoom-out"] = QAction("Zoo&m Out", self)
        self._actions["zoom-out"].setShortcut(QKeySequence.ZoomOut)
        self._actions["zoom-out"].setIcon(QIcon.fromTheme("zoom-out"))

    def getStandardAction(self, name):
        return self._actions[str(name).lower()]
