#!/bin/sh

for plugin in "mandelbrot" "newton" "ifs"
do
    case $1 in
        make)
            echo "-----------------------------------------------"
            echo "installing $plugin"
            cd "plugins/$plugin/"
            ./setup.sh make
            cp *lib*so* ../../
            ;;
        clean)
            echo "-----------------------------------------------"
            echo "cleaning $plugin"
            cd "plugins/$plugin/"
            ./setup.sh clean
            ;;
        *)
            echo "Usage: ./install.sh [make|clean]"
            return 0
            ;;
    esac
    cd -
done

if [ "$1" == "clean" ]
then
    echo "cleaning main"
    pwd
    find -regex '.+\(pyc\|~\|so\|old\)$' | xargs rm -v
    rm -v lib*so*
fi