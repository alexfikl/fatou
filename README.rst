Fatou
=====

This is meant to be a fractal (and related objects) visualizer.

It currently features:

* Cellular Automata (Game of Life, Elementary Automata).
* Iterated Function Systems (Barnsley's fern, etc and the possibility for user
  input)
* Lindenmayer Systems (most of the more well known ones and user input)
* Mandelbrot and Julia Fractals
* Newton Fractals (user input for formulas)

A more detailed description of all these objects and the structure of the
application can be found in the `docs/` folder.

Caution
=======

This was written as my Bachelor's Degree thesis in 2011-ish and it contained a
mishmash of Python, Qt4 and ``sip`` and other related tools that were sort of
made to work together enough to make an application that starts and does the
visualization.

It has since been updated to Qt5 and Qt6 with no care for actually working
properly. Some small testing revealed a lot of flickering and buttons not quite
working as intended.

With all that in mind, the current state of the project is: it can open a window
and render some L-systems. That's it! You're probably better off starting from
scratch :(

Installation
============

This uses pretty recent (as of 2025) Python packaging guidelines, so it should
install in the usual way. See ``pyproject.toml`` for dependencies and
``requirements.txt`` for the latest version this was tested with.

.. code:: bash

    python -m pip install --editable .

License
=======

The code is licensed under the ``GPL-2.0-or-later`` license (see
``LICENSES/GPL-2.0-or-later.txt`` for additional information).
