Main
====

- fractal dimension/area thing
- Lorenz attractor and other dynamical systems
- landscape generation
- more advanced coloring for all
- same shortcuts and stuff everywhere
- add tooltips to help
- remake color dialog to use QColorDialog
- make plugin selection dialog able to install plugins

Default
=======

- make it prettier

Mandelbrot
==========

- mandelbrot user submitted formulas
- refactor somehow to share more stuff with Newton

Newton
======

- make a better equation parser/solver

Cellular
========

- option to autoresize if margins reached
- look into the hashlife algorithm.

L-systems
=========

- add functions
- add more options

IFS
===
- interface for user submitted maps
