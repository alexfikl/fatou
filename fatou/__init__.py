# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from importlib import metadata

m = metadata.metadata("fatou")
__name__ = m["Name"]
author = m["Author"]
__version__ = m["Version"]
