# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtCore import QLineF, QPointF, Qt, pyqtSignal
from PyQt6.QtGui import QColor, QImage, QLinearGradient, QPainter, QPolygonF
from PyQt6.QtWidgets import QSizePolicy, QVBoxLayout, QWidget

from fatou.gradients.hoverpoints import HoverPoints


class ShadeWidget(QWidget):
    """Class providing a certain color for the editor.
    Available colors are Red, Green, Blue.
    """

    colorsChanged = pyqtSignal(QPolygonF)

    RedShade = 1
    GreenShade = 2
    BlueShade = 3

    def __init__(self, shade_type, parent=None):
        super().__init__(parent)

        self._shade_type = shade_type
        self._shade = QImage()

        self.setAttribute(Qt.WidgetAttribute.WA_OpaquePaintEvent)

        points = QPolygonF()
        points.append(QPointF(0, 0))
        points.append(QPointF(self.size().width(), 0))

        self._hoverPoints = HoverPoints(self)
        self._hoverPoints.setPoints(points)

        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self._hoverPoints.pointsChanged.connect(self.colorsChanged)

    def points(self):
        """Returns the hover points currently on the widget.
        The points are part of a QPolygonF.
        """
        return self._hoverPoints.points()

    def setHoverPoints(self, points):
        """Sets new hover points. The points must be a QPolygonF."""
        self._hoverPoints.setPoints(points)

    def colorAt(self, x):
        """Returns the color at a certain width.
        Iterates through all the available hover points until it finds the
        one closest to the given point.
        """
        self.__generateShade()

        pts = self._hoverPoints.points()

        for i in range(len(pts)):
            if pts[i - 1].x() < x and pts[i].x() >= x:
                line = QLineF(pts[i - 1], pts[i])
                line.setLength((line.length() * (x - line.x1())) / line.dx())

                px = int(min(line.x2(), self._shade.width() - 1))
                py = int(min(line.y2(), self._shade.height() - 1))
                return self._shade.pixel(px, py)

        return 0

    def paintEvent(self, _):
        """Implemented virtual paintEvent function.
        Generates the gradient and then paints it on the image.
        """
        self.__generateShade()

        painter = QPainter(self)
        painter.drawImage(0, 0, self._shade)
        painter.setPen(QColor(146, 146, 146))
        painter.drawRect(0, 0, self.width() - 1, self.height() - 1)

    def __generateShade(self):
        if self._shade.isNull() or self._shade.size() != self.size():
            self._shade = QImage(self.size(), QImage.Format_RGB32)
            shade = QLinearGradient(0, 0, 0, self.height())
            shade.setColorAt(1, Qt.GlobalColor.black)

            if self._shade_type == self.RedShade:
                shade.setColorAt(0, Qt.GlobalColor.red)
            elif self._shade_type == self.GreenShade:
                shade.setColorAt(0, Qt.GlobalColor.green)
            else:
                shade.setColorAt(0, Qt.GlobalColor.blue)

            painter = QPainter(self._shade)
            painter.fillRect(self.rect(), shade)


class GradientEditor(QWidget):
    """A color picker class in the form of a gradient editor.
    Composed of three gradient widgets (ShadeWidget) one for
    each available color.
    """

    gradientStopsChanged = pyqtSignal(list)

    def __init__(self, parent=None):
        super().__init__(parent)

        vbox = QVBoxLayout(self)
        vbox.setSpacing(1)
        vbox.setMargin(1)

        self._redShade = ShadeWidget(ShadeWidget.RedShade, self)
        self._greenShade = ShadeWidget(ShadeWidget.GreenShade, self)
        self._blueShade = ShadeWidget(ShadeWidget.BlueShade, self)

        vbox.addWidget(self._redShade)
        vbox.addWidget(self._greenShade)
        vbox.addWidget(self._blueShade)

        self._redShade.colorsChanged.connect(self.__pointsUpdated)
        self._greenShade.colorsChanged.connect(self.__pointsUpdated)
        self._blueShade.colorsChanged.connect(self.__pointsUpdated)

        self.setMinimumSize(300, 150)

    def setGradientStops(self, new_stops):
        """Used to transform from QGradientStop to QPolygonF for each color."""
        pts = {i: QPolygonF() for i in ["red", "gr", "blue"]}

        for sp in new_stops:
            red, green, blue = sp[1].getRgb()[:3]

            redw, redh = self._redShade.width(), self._redShade.height()
            grw, grh = self._greenShade.width(), self._greenShade.height()
            blw, blh = self._blueShade.width(), self._blueShade.height()

            pts["red"].append(QPointF(sp[0] * redw, redh - red * redh / 255.0))
            pts["gr"].append(QPointF(sp[0] * grw, grh - green * grh / 255.0))
            pts["blue"].append(QPointF(sp[0] * blw, blh - blue * blh / 255.0))

        setShadePoints(pts["red"], self._redShade)
        setShadePoints(pts["gr"], self._greenShade)
        setShadePoints(pts["blue"], self._blueShade)

    def __pointsUpdated(self):
        w = float(self._redShade.width())

        points = QPolygonF()

        stops = []
        points += self._redShade.points()
        points += self._greenShade.points()
        points += self._blueShade.points()

        points = QPolygonF(sorted(points, key=lambda p: p.x()))

        for i in range(len(points)):
            x = int(points.at(i).x())
            if i < (len(points) - 1) and x == points.at(i + 1).x():
                continue

            r = (0x00FF0000 & self._redShade.colorAt(int(x))) >> 16
            g = (0x0000FF00 & self._greenShade.colorAt(int(x))) >> 8
            b = 0x000000FF & self._blueShade.colorAt(int(x))
            color = QColor(r, g, b)
            x = round(x / w, 2)
            if x > 1:
                return 0

            if not any(abs(x - r) < 0.1e-3 for r, color in stops):
                stops.append((x, color))

        self.gradientStopsChanged.emit(stops)


def setShadePoints(points, shade_widget):
    """Sets new hover points for a specific ShadeWidget."""
    shade_widget.setHoverPoints(points)
    shade_widget.update()
