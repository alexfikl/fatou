# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtCore import QEvent, QObject, QPointF, QRectF, QSize, Qt, pyqtSignal
from PyQt6.QtGui import QBrush, QColor, QPainter, QPainterPath, QPen, QPolygonF
from PyQt6.QtWidgets import QApplication


class HoverPoints(QObject):
    """Class that provides the representation of a list of hover points on
    top of a ShadeWidget.
    """

    pointsChanged = pyqtSignal(QPolygonF)

    NoLock = 0
    LockToLeft = 1
    LockToRight = 2

    def __init__(self, widget):
        QObject.__init__(self, widget)

        self._widget = widget
        self._widget.installEventFilter(self)
        self._pointSize = QSize(11, 11)
        self._hoverPoints = QPolygonF()
        self._currentIndex = -1

    def points(self):
        """Returns current hover points."""
        return self._hoverPoints

    def setPoints(self, pts):
        """Sets new hover points. The left-most and right-most points are
        locked on their respective side.
        """
        hover_points = [self.__boundPoint(pts[0], HoverPoints.LockToLeft)]
        hover_points.extend([self.__boundPoint(i, 0) for i in pts[1:-1]])
        hover_points.append(self.__boundPoint(pts[-1], HoverPoints.LockToRight))

        self._hoverPoints = QPolygonF(hover_points)

    def eventFilter(self, obj, ev):
        """Implementation of the virtual function. Handles all events sent to
        the object.
        """
        if obj == self._widget:
            switch = {
                QEvent.MouseButtonPress: self.__handleMouseButtonPress,
                QEvent.MouseButtonRelease: self.__handleMouseButtonRelease,
                QEvent.MouseMove: self.__handleMouseMove,
                QEvent.Resize: self.__handleResize,
                QEvent.Paint: self.__handlePaint,
            }

            return switch.get(ev.type(), lambda ev: False)(ev)

        return False

    def __pointBoundingRect(self, point):
        w = self._pointSize.width()
        h = self._pointSize.height()

        return QRectF(point.x() - w / 2.0, point.y() - h / 2.0, w, h)

    def __handleMouseButtonPress(self, ev):
        click_pos = QPointF(ev.pos())
        index = -1
        i = 0

        for i, point in enumerate(self._hoverPoints):
            path = QPainterPath()
            path.addEllipse(self.__pointBoundingRect(point))

            if path.contains(click_pos):
                index = i
                break

        if ev.button() == Qt.MouseButton.LeftButton:
            if index == -1:
                pos = 0

                for point in self._hoverPoints:
                    if point.x() > click_pos.x():
                        pos = i
                        break

                self._hoverPoints.insert(pos, click_pos)
                self._currentIndex = pos
                self.__firePointChange(True)
            else:
                self._currentIndex = index
            return True
        elif ev.button() == Qt.MouseButton.RightButton:
            if index > 0 and index < (len(self._hoverPoints) - 1):
                self._hoverPoints.remove(index)
                self.__firePointChange(True)
                return True

        return False

    def __handleMouseButtonRelease(self, _):
        self._currentIndex = -1
        self.__firePointChange(True)

        return False

    def __handleMouseMove(self, ev):
        if self._currentIndex >= 0:
            self.__movePoint(self._currentIndex, ev.pos(), False)

        return False

    def __handleResize(self, ev):
        if not ev.oldSize().isEmpty():
            stretchx = float(ev.size().width()) / ev.oldSize().width()
            stretchy = float(ev.size().height()) / ev.oldSize().height()

            for i, pt in enumerate(self._hoverPoints):
                np = QPointF(pt.x() * stretchx, pt.y() * stretchy)
                self.__movePoint(i, np, False)

            self.__firePointChange(True)

        return False

    def __handlePaint(self, ev):
        current_widget = self._widget
        self._widget = None
        QApplication.sendEvent(current_widget, ev)
        self._widget = current_widget
        self.__paintPoints()

        return True

    def __paintPoints(self):
        painter = QPainter()
        painter.begin(self._widget)
        painter.setRenderHint(QPainter.Antialiasing)

        painter.setPen(QPen(QColor(255, 255, 255, 127), 2))

        path = QPainterPath()
        path.moveTo(self._hoverPoints[0])

        for i in range(1, len(self._hoverPoints)):
            p1 = self._hoverPoints[i - 1]
            p2 = self._hoverPoints[i]
            distance = p2.x() - p1.x()

            path.cubicTo(
                p1.x() + distance / 2,
                p1.y(),
                p1.x() + distance / 2,
                p2.y(),
                p2.x(),
                p2.y(),
            )

        painter.drawPath(path)

        painter.setPen(QPen(QColor(255, 255, 255, 191), 1))
        painter.setBrush(QBrush(QColor(191, 191, 191, 127)))

        for point in self._hoverPoints:
            painter.drawEllipse(self.__pointBoundingRect(point))

    def __boundPoint(self, point, lock):
        left = self._widget.rect().left()
        right = self._widget.rect().right()
        top = self._widget.rect().top()
        bottom = self._widget.rect().bottom()

        if point.x() < left or lock == HoverPoints.LockToLeft:
            point.setX(left)
        elif point.x() > right or lock == HoverPoints.LockToRight:
            point.setX(right)

        if point.y() < top:
            point.setY(top)
        elif point.y() > bottom:
            point.setY(bottom)

        return QPointF(point)

    def __movePoint(self, index, point, emit_update):
        if index == 0:
            lock = HoverPoints.LockToLeft
        elif index == (len(self._hoverPoints) - 1):
            lock = HoverPoints.LockToRight
        else:
            lock = HoverPoints.NoLock

        self._hoverPoints[index] = self.__boundPoint(point, lock)

        self.__firePointChange(emit_update)

    def __firePointChange(self, emit_update):
        old_current = QPointF()
        if self._currentIndex != -1:
            old_current = self._hoverPoints[self._currentIndex]

        self._hoverPoints = sorted(self._hoverPoints, key=lambda p: p.x())
        self._hoverPoints = QPolygonF(self._hoverPoints)

        if self._currentIndex != -1:
            for index, point in enumerate(self._hoverPoints):
                if point == old_current:
                    self._currentIndex = index
                    break

        self._widget.update()
        if emit_update:
            self.pointsChanged.emit(self._hoverPoints)
