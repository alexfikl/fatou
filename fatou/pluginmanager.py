# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from dataclasses import dataclass
from importlib.metadata import entry_points

from PyQt6.QtCore import QObject, pyqtSignal

from fatou.plugins.abstractfractal import AbstractFractalExplorer

FATOU_PLUGIN_GROUP = "fatou.plugins"


@dataclass(frozen=True)
class Plugin:
    name: str
    entrypoint: AbstractFractalExplorer


class PluginManager(QObject):
    pluginListChanged = pyqtSignal(list, int)

    def __init__(self) -> None:
        super().__init__()

        self.plugins = {}
        self.loadPlugins()

    def getPlugin(self, plugin_id: str) -> Plugin:
        if plugin_id in self.plugins:
            return self.plugins[plugin_id]
        else:
            raise Plugin(name="", entrypoint=None)

    def refresh(self):
        self.loadPlugins()

    def listPlugins(self):
        return list(self.plugins.values())

    def loadPlugins(self):
        entrypoints = entry_points(group=FATOU_PLUGIN_GROUP)
        for ep in entrypoints:
            self.plugins[ep.name] = Plugin(name=ep.name, entrypoint=ep.load())
