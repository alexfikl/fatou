# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from dataclasses import dataclass

from PyQt6.QtCore import QSettings, Qt, pyqtSignal
from PyQt6.QtWidgets import (
    QApplication,
    QComboBox,
    QFormLayout,
    QGroupBox,
    QHBoxLayout,
    QLineEdit,
    QMessageBox,
    QPushButton,
    QSpinBox,
    QTableWidget,
    QTableWidgetItem,
    QVBoxLayout,
)

from fatou.plugins.abstractfractal import AbstractFractalOptions

# {{{ presets


@dataclass(frozen=True)
class Preset:
    name: str
    angle: int
    its: int
    axiom: str
    rules: dict[str, str]
    rotate: int


LINDENMAYER_SYSTEMS_PRESETS = [
    Preset(
        name="Levy Dragon",
        angle=90,
        its=10,
        axiom="FX",
        rules={"X": "X+YF+", "Y": "-FX-Y"},
        rotate=0,
    ),
    Preset(
        name="Koch Snowflake",
        angle=60,
        its=4,
        axiom="F++F++F",
        rules={"F": "F-F++F-F"},
        rotate=0,
    ),
    Preset(
        name="Levy C",
        angle=45,
        its=10,
        axiom="F",
        rules={"F": "+F--F+"},
        rotate=-45,
    ),
    Preset(
        name="Koch Curve",
        angle=90,
        its=3,
        axiom="F-F-F-F",
        rules={"F": "FF-F-F-F-F-F+F"},
        rotate=0,
    ),
    Preset(
        name="Sierpinski Triangle",
        angle=60,
        its=7,
        axiom="A",
        rules={"A": "B-A-B", "B": "A+B+A"},
        rotate=180,
    ),
    Preset(
        name="Sierpinski Median",
        angle=45,
        its=10,
        axiom="L--F--L--F",
        rules={"L": "+R-F-R+", "R": "-L+F+L-"},
        rotate=45,
    ),
    Preset(
        name="Peano",
        angle=90,
        its=3,
        axiom="X",
        rules={"X": "XFYFX+F+YFXFY-F-XFYFX", "Y": "YFXFY-F-XFYFX+F+YFXFY"},
        rotate=0,
    ),
    Preset(
        name="Square Sierpinski",
        angle=90,
        its=3,
        axiom="F+XF+F+XF",
        rules={"X": "XF-F+F-XF+F+XF-F+F-X"},
        rotate=0,
    ),
    Preset(
        name="Islands and Lakes",
        angle=90,
        its=2,
        axiom="F+F+F+F",
        rules={"F": "F+f-FF+F+FF+Ff+FF-f+FF-F-FF-Ff-FFF", "f": "ffffff"},
        rotate=0,
    ),
    Preset(
        name="Koch Cubes",
        angle=90,
        its=3,
        axiom="F-F-F-F",
        rules={"F": "FF-F-F-F-FF"},
        rotate=0,
    ),
    Preset(
        name="Koch Moss",
        angle=90,
        its=4,
        axiom="F-F-F-F",
        rules={"F": "FF-F--F-F"},
        rotate=0,
    ),
    Preset(
        name="Plant 1",
        angle=25,
        its=5,
        axiom="X",
        rules={"X": "F-[[X]+X]+F[+FX]-X", "F": "FF"},
        rotate=240,
    ),
    Preset(
        name="Plant 2",
        angle=7,
        its=6,
        axiom="+++++++++++++X",
        rules={"X": "F[@.5+++++++++!X]-F[@.4-----------X]@.6X"},
        rotate=180,
    ),
    Preset(
        name="Plant 3",
        angle=26,
        its=6,
        axiom="X",
        rules={"X": "F[+X][-X]FX", "F": "FF"},
        rotate=-117,
    ),
]

LINDENMAYER_SYSTEMS_PRESET_NAMES = [p.name for p in LINDENMAYER_SYSTEMS_PRESETS]

# }}}


# {{{ RulesEdit


class RulesEdit(QTableWidget):
    rulesChanged = pyqtSignal(dict)

    def __init__(self, rules, parent=None):
        super().__init__(parent)
        self._settingRules = False

        self.verticalHeader().hide()
        self.setColumnCount(2)
        self.setRowCount(len(rules))
        self.setColumnWidth(0, 40)
        self.setHorizontalHeaderLabels(["From", "To"])
        self.setRules(rules)

        self.cellChanged.connect(self.toUpper)

    def toUpper(self, row, column):
        item = self.item(row, column)
        item.setData(
            Qt.ItemDataRole.DisplayRole, item.data(Qt.ItemDataRole.DisplayRole).upper()
        )

        if not column:
            item.setTextAlignment(Qt.AlignmentFlag.AlignCenter)

        if not self._settingRules:
            self.rulesChanged.emit(self.getRules())

    def addRow(self):
        row = self.rowCount()
        self.insertRow(row)

    def getRules(self):
        rules = {}
        for i in range(self.rowCount()):
            first = str(self.item(i, 0).data(Qt.ItemDataRole.DisplayRole))
            last = str(self.item(i, 1).data(Qt.ItemDataRole.DisplayRole))
            if first and last:
                rules[first] = last
        return rules

    def setRules(self, rules):
        self._settingRules = True
        self.clearRules()
        self.setRowCount(len(rules))

        for i, (key, value) in enumerate(rules.items()):
            item = QTableWidgetItem(key)
            item.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
            self.setItem(i, 0, item)

            item = QTableWidgetItem(value)
            self.setItem(i, 1, item)

        self._settingRules = False

    def resizeEvent(self, ev):
        col_size = self.size().width() - 45
        self.setColumnWidth(1, col_size)

    def isEmpty(self):
        for i in range(self.rowCount()):
            try:
                first = str(self.item(i, 0).data(Qt.ItemDataRole.DisplayRole))
                last = str(self.item(i, 1).data(Qt.ItemDataRole.DisplayRole))
                if last and first:
                    return False
            except AttributeError:
                pass

        return True

    def clearRules(self):
        self.clear()
        self.setRowCount(1)
        self.setHorizontalHeaderLabels(["From", "To"])


# }}}


# {{{ LindenmayerSystemsOptions


class LindenmayerSystemsOptions(AbstractFractalOptions):
    saveToFile = pyqtSignal(str)
    iterationsChanged = pyqtSignal(int)
    axiomChanged = pyqtSignal(str)
    angleChanged = pyqtSignal(int)
    rulesChanged = pyqtSignal(dict)
    rotationChanged = pyqtSignal(int)
    start = pyqtSignal()
    stop = pyqtSignal()

    def __init__(self, parent=None):
        AbstractFractalOptions.__init__(self, parent)

        self.readSettings()
        self.createWidgets()
        self.createLayout()

        QApplication.instance().aboutToQuit.connect(self.saveSettings)
        self.setMinimumWidth(160)
        self.setMaximumWidth(220)

    def readSettings(self):
        settings = QSettings("fatou", "fatou")

        settings.beginGroup("lsystems")
        self._current = int(settings.value("current", 5))
        self._angleV = int(settings.value("angle", 45))
        self._iterationsV = int(settings.value("iterations", 10))
        self._rotation = int(settings.value("rotation", 0))
        self._axiomV = settings.value("axiom", "L--F--L--F")
        rules = settings.value("rules", "L:+R-F-R+;;R:-L+F+L-")
        self._rulesV = dict([x.split(":") for x in rules.split(";;")])
        settings.endGroup()

    def saveSettings(self):
        settings = QSettings("fatou", "fatou")

        rules = self._rules.getRules()
        rules = ";;".join([":".join(x) for x in list(rules.items())])

        settings.beginGroup("lsystems")
        settings.setValue("current", self._presets.currentIndex())
        settings.setValue("angle", self._angle.value())
        settings.setValue("iterations", self._iterations.value())
        settings.setValue("rotation", self._rotation)
        settings.setValue("axiom", self._axiom.text())
        settings.setValue("rules", rules)
        settings.endGroup()

    def createWidgets(self):
        self._presets = QComboBox(self)
        for f in LINDENMAYER_SYSTEMS_PRESETS:
            self._presets.addItem(f.name)
        self._presets.activated.connect(self.__setPresetValues)
        self._presets.setCurrentIndex(self._current)

        self._iterations = QSpinBox(self)
        self._iterations.setRange(1, 100)
        self._iterations.setValue(self._iterationsV)
        self._iterations.valueChanged.connect(self.iterationsChanged)

        self._axiom = QLineEdit(self)
        self._axiom.setText(self._axiomV)
        self._axiom.textChanged.connect(self.axiomChanged)

        self._angle = QSpinBox(self)
        self._angle.setRange(0, 360)
        self._angle.setValue(self._angleV)
        self._angle.valueChanged.connect(self.angleChanged)

        self._rules = RulesEdit(self._rulesV, self)
        self._rules.rulesChanged.connect(self.rulesChanged)

        self._addRow = QPushButton("Add Rule")
        self._addRow.clicked.connect(self._rules.addRow)

        self._clearRules = QPushButton("Clear Rules")
        self._addRow.clicked.connect(self._rules.clearRules)

    def createLayout(self):
        main_layout = QVBoxLayout(self)

        layout = QFormLayout()
        layout.addRow("Iterations:", self._iterations)
        layout.addRow("Axiom:", self._axiom)
        layout.addRow("Angle:", self._angle)

        group = QGroupBox("Rules")
        l1 = QVBoxLayout()
        l1.addWidget(self._rules)
        l2 = QHBoxLayout()
        l2.addWidget(self._addRow)
        l2.addWidget(self._clearRules)
        l1.addLayout(l2)
        group.setLayout(l1)

        main_layout.addWidget(self._presets)
        main_layout.addLayout(layout)
        main_layout.addWidget(group)
        main_layout.addStretch()

        self.setLayout(main_layout)

    def __checker(self):
        if self._axiom.text().isEmpty():
            QMessageBox.warning(
                self, "Warning", "There is no starting variable defined for the system."
            )
        elif self._rules.isEmpty():
            QMessageBox.warning(
                self, "Warning", "There are no rules defined for the system."
            )
        elif not any(
            f in self._axiom.text() for f, t in self._rules.getRules().items()
        ):
            QMessageBox.warning(
                self,
                "Warning",
                "Starting variable cannot be found in any defined rule.",
            )
        else:
            self.emitStartData()

    def emitStartData(self):
        self.angleChanged.emit(self._angle.value())
        self.iterationsChanged.emit(self._iterations.value())
        self.axiomChanged.emit(self._axiom.text())
        self.rulesChanged.emit(self._rules.getRules())
        self.rotationChanged.emit(self._rotation)
        self.start.emit()

    def __setPresetValues(self, index: int) -> None:
        current = LINDENMAYER_SYSTEMS_PRESETS[index]
        self._angle.setValue(current.angle)
        self._iterations.setValue(current.its)
        self._axiom.setText(current.axiom)
        self._rules.setRules(current.rules)
        self._rotation = current.rotate


# }}}
