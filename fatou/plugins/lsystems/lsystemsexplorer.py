# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtCore import pyqtSignal
from PyQt6.QtWidgets import QHBoxLayout

from fatou.plugins.abstractfractal import AbstractFractalExplorer
from fatou.plugins.lsystems.lsystemsoptions import (
    LINDENMAYER_SYSTEMS_PRESET_NAMES,
    LindenmayerSystemsOptions,
)
from fatou.plugins.lsystems.lsystemsview import LindenmayerSystemsView


class LindenmayerSystemsExplorer(AbstractFractalExplorer):
    display_name = "Lindenmayer Systems"
    presets = LINDENMAYER_SYSTEMS_PRESET_NAMES

    saveTo = pyqtSignal(str)
    start = pyqtSignal()
    stopped = pyqtSignal()
    zoomIn = pyqtSignal()
    zoomOut = pyqtSignal()

    iterationsChanged = pyqtSignal(int)
    axiomChanged = pyqtSignal(str)
    angleChanged = pyqtSignal(int)
    rulesChanged = pyqtSignal(dict)

    def __init__(self, parent=None) -> None:
        super().__init__(parent)

        view = LindenmayerSystemsView(self)
        options = LindenmayerSystemsOptions(self)

        layout = QHBoxLayout()
        layout.addWidget(view)
        layout.addWidget(options)
        layout.setContentsMargins(0, 0, 0, 0)

        self.saveTo.connect(view.saveToFile)
        self.start.connect(options.emitStartData)
        self.zoomIn.connect(view.zoomIn)
        self.zoomOut.connect(view.zoomOut)

        view.stopped.connect(self.stopped)

        options.iterationsChanged.connect(view.iterationsChanged)
        options.angleChanged.connect(view.angleChanged)
        options.axiomChanged.connect(view.axiomChanged)
        options.rulesChanged.connect(view.rulesChanged)
        options.rotationChanged.connect(view.setRotation)
        options.start.connect(view.start)

        self.view = view
        self.options = options
        self.setLayout(layout)
