# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtCore import QObject, QRectF, Qt, pyqtSignal
from PyQt6.QtGui import QImage, QPainter
from PyQt6.QtWidgets import QGraphicsItem, QGraphicsScene, QGraphicsView

from fatou.plugins.lsystems.lsystemsitem import LindenmayerSystemsItem


class Link(QObject):
    stopped = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)


class LindenmayerSystemsView(QGraphicsView):
    saveToFile = pyqtSignal(str)
    axiomChanged = pyqtSignal(str)
    rulesChanged = pyqtSignal(dict)
    angleChanged = pyqtSignal(int)
    iterationsChanged = pyqtSignal(int)
    start = pyqtSignal()
    stopped = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)

        self._link = Link()
        self._link.stopped.connect(self.__rotate)
        self._link.stopped.connect(self.stopped)
        self._rotation = 0

        self._scene = QGraphicsScene()
        self.setScene(self._scene)
        self.setBackgroundBrush(Qt.GlobalColor.black)
        self._scene.setSceneRect(0, 0, 600, 600)
        self._scene.setItemIndexMethod(QGraphicsScene.ItemIndexMethod.NoIndex)
        self.setViewportUpdateMode(
            QGraphicsView.ViewportUpdateMode.BoundingRectViewportUpdate
        )
        self.setRenderHint(QPainter.RenderHint.Antialiasing)

        self._litem = LindenmayerSystemsItem(self._link)
        self._litem.setFlag(QGraphicsItem.GraphicsItemFlag.ItemIsMovable)

        self.axiomChanged.connect(self._litem.setAxiom)
        self.rulesChanged.connect(self._litem.setRules)
        self.angleChanged.connect(self._litem.setAngle)
        self.iterationsChanged.connect(self._litem.setIterations)
        self.start.connect(self._litem.start)

        self.saveToFile.connect(self.saveTo)

        self._litem.setPos(0, 0)
        self._scene.addItem(self._litem)

    def setRotation(self, rotation):
        if self._rotation:
            self.rotate(-self._rotation)
        self._rotation = rotation
        self._rotate = False

    def saveTo(self, filename):
        # TODO: offer option for image size
        image = QImage(512, 512, QImage.Format.Format_Indexed8)
        p = QPainter(image)
        p.setRenderHint(QPainter.RenderHint.Antialiasing)
        self._scene.render(p)
        p.end()

        image.save(filename, "PNG")

    def wheelEvent(self, ev):
        self.scaleView(2 ** (ev.angleDelta().y() / 240.0))

    def zoomIn(self):
        self.scaleView(1.4)

    def zoomOut(self):
        self.scaleView(0.7)

    def keyPressEvent(self, ev):
        # TODO: more moving around stuff
        if ev.key() == Qt.Key.Key_Plus:
            self.scaleView(1.2)
        if ev.key() == Qt.Key.Key_Minus:
            self.scaleView(1 / 1.2)
        QGraphicsView.keyPressEvent(self, ev)

    def scaleView(self, scale_factor):
        print(scale_factor)
        factor = self.transform().scale(scale_factor, scale_factor)
        factor = factor.mapRect(QRectF(0, 0, 1, 1)).width()

        if factor < 0.07 or factor > 100:
            return
        self.scale(scale_factor, scale_factor)

    def __rotate(self):
        if not self._rotate:
            self.rotate(self._rotation)
            self._rotate = True
