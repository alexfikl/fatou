# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtCore import QPoint, QPointF, QSize, Qt, QThread, pyqtSignal
from PyQt6.QtGui import QPainter, QPixmap
from PyQt6.QtWidgets import QWidget

# from fatou.plugins.mandelbrot.renderer import MandelbrotRenderer


class MandelbrotWidget(QWidget):
    juliaCenterChanged = pyqtSignal(float, float)
    information = pyqtSignal(str)
    stopped = pyqtSignal()
    render = pyqtSignal(QSize, int)
    color = pyqtSignal(QSize)

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self._zoomFactor = {"in": 0.8, "out": 1.25}
        self._scrollStep = 20
        self._pixmap = QPixmap()
        self._pixmapOffset = QPointF()
        self._lastDragPosition = QPointF()
        self._currentScale = 0.0078125
        self._type = 0

        # this logic is probably flawed
        # make a thread -> move the object to it
        # when we tell the object to render it starts the thread
        # when the rendering is complete it stops it
        self._thread = QThread()
        # self._renderer = MandelbrotRenderer()
        # self._renderer.moveToThread(self._thread)
        self.render.connect(self._thread.start)
        self.color.connect(self._thread.start)
        self.render.connect(self._renderer.render)
        self.color.connect(self._renderer.color)
        self._renderer.completed.connect(self.__updatePixmap)
        self._renderer.completed.connect(self._thread.quit)
        self._renderer.completed.connect(self.stopped)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.fillRect(self.rect(), Qt.GlobalColor.black)

        if self._pixmap.isNull():
            painter.setPen(Qt.GlobalColor.white)
            painter.drawText(
                self.rect(),
                Qt.AlignmentFlag.AlignCenter,
                "Rendering initial image, please wait.",
            )
            return

        if self._currentScale == self._renderer.scale():
            painter.drawPixmap(self._pixmapOffset, self._pixmap)
        else:
            scale_factor = self._renderer.scale() / self._currentScale
            new_width = int(self._pixmap.width() * scale_factor)
            new_height = int(self._pixmap.height() * scale_factor)
            new_x = self._pixmapOffset.x() + (self._pixmap.width() - new_width) / 2
            new_y = self._pixmapOffset.y() + (self._pixmap.height() - new_height) / 2

            painter.save()
            painter.translate(new_x, new_y)
            painter.scale(scale_factor, scale_factor)
            exposed = painter.matrix().inverted()[0]
            exposed = exposed.mapRect(self.rect().adjusted(-1.0, -1.0, 1.0, 1.0))
            painter.drawPixmap(exposed, self._pixmap, exposed)
            painter.restore()

    def resizeEvent(self, event):
        self._renderer.abort()
        self.render.emit(self.size(), self._type)

    def keyPressEvent(self, event):
        key = event.key()

        if key == Qt.Key.Key_Plus:
            self.__zoom(self._zoomFactor["in"])
        elif key == Qt.Key.Key_Minus:
            self.__zoom(self._zoomFactor["out"])
        elif key == Qt.Key.Key_Left:
            self.__scroll(-self._step, 0)
        elif key == Qt.Key.Key_Right:
            self.__scroll(self._step, 0)
        elif key == Qt.Key.Key_Down:
            self.__scroll(0, -self._step)
        elif key == Qt.Key.Key_Up:
            self.__scroll(0, self._step)
        else:
            QWidget.keyPressEvent(self, event)

    def wheelEvent(self, event):
        num_degrees = event.delta() / 8
        num_steps = num_degrees / 15.0
        self.__zoom(self._zoomFactor["in"] ** num_steps)

    def mousePressEvent(self, event):
        if event.button() == Qt.MouseBoutton.LeftButton:
            # change the coordinates in options when we click on something
            # and the julia option is on

            self._lastDragPosition = event.pos()
        if event.button() == Qt.MouseButton.RightButton and self._julia:
            posx = event.pos().x() - self.width() / 2
            posy = event.pos().y() - self.height() / 2
            x = self._renderer.centerX() + posx * self._currentScale
            y = self._renderer.centerY() + posy * self._currentScale
            self.juliaCenterChanged.emit(x, y)

    def mouseMoveEvent(self, event):
        self._renderer.abort()
        if event.buttons() == Qt.MouseButton.LeftButton:
            self._pixmapOffset += event.pos() - self._lastDragPosition
            self._lastDragPosition = event.pos()
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.MouseButton.LeftButton:
            self._pixmapOffset += event.pos() - self._lastDragPosition
            self._lastDragPosition = QPoint()

            deltax = (self.width() - self._pixmap.width()) / 2 - self._pixmapOffset.x()
            deltay = (
                self.height() - self._pixmap.height()
            ) / 2 - self._pixmapOffset.y()
            self.__scroll(deltax, deltay)

    def __updatePixmap(self, image, scale):
        if not self._lastDragPosition.isNull():
            return

        self._pixmap = QPixmap.fromImage(image)
        self._pixmapOffset = QPoint()
        self._lastDragPosition = QPoint()
        self.update()

    def saveTo(self, filename):
        image = self._pixmap.toImage()
        image.save(filename)

    def start(self):
        self.render.emit(self.size(), self._type)

    def stop(self):
        self._renderer.abort()

    def zoomIn(self):
        self.__zoom(self._zoomFactor["in"])

    def zoomOut(self):
        self.__zoom(self._zoomFactor["out"])

    def __zoom(self, zoom):
        # update scale
        self._currentScale *= zoom

        # update widget
        self.update()

        # tell the renderer the new scale and start rendering
        self._renderer.setScale(self._currentScale)
        self.render.emit(self.size(), self._type)

        x = self._renderer.centerX()
        y = self._renderer.centerY()
        i = f"Center: ({x:.3f}, {y:.3f}) Scale: {self._currentScale:.3f}"
        self.information.emit(i)

    def __scroll(self, deltax, deltay):
        # get new coordinates
        x = self._renderer.centerX() + deltax * self._currentScale
        y = self._renderer.centerY() + deltay * self._currentScale

        # send coordinates to renderer
        self._renderer.setCenterCoordinates(x, y)
        self.update()

        # emit the new information
        i = f"Center: ({x:.3f}, {y:.3f}) Scale: {self._currentScale:.3f}"
        self.information.emit(i)

        # tell the render to render with the new center
        self.render.emit(self.size(), self._type)

    def setIterations(self, its):
        self._renderer.setIterations(its)

    def setColorDensity(self, density, color):
        self._renderer.setColorDensity(density)
        if color:
            self.color.emit(self.size())

    def setCenterX(self, x):
        self._renderer.setCenterX(x)

    def setCenterY(self, y):
        self._renderer.setCenterY(y)

    def setJuliaX(self, x):
        self._renderer.setJuliaX(x)

    def setJuliaY(self, y):
        self._renderer.setJuliaY(y)

    def setColorMapSize(self, size, color):
        self._renderer.setColorMapSize(size)
        if color:
            self.color.emit(self.size())

    def setGradientStops(self, stops, color):
        self._renderer.setGradientStops(stops)
        if color:
            self.color.emit(self.size())

    def setJulia(self, julia, send):
        self._julia = julia
        if send:
            self._renderer.setJulia(julia)

    def setSmoothColoring(self, smooth, color):
        self._renderer.setSmoothColoring(smooth != 0)
        if color:
            self.color.emit(self.size())

    def setType(self, mtype):
        self._type = mtype
