# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from dataclasses import dataclass

from PyQt6.QtCore import QSettings, Qt, pyqtSignal
from PyQt6.QtGui import QColor
from PyQt6.QtWidgets import (
    QCheckBox,
    QComboBox,
    QDoubleSpinBox,
    QFormLayout,
    QGroupBox,
    QPushButton,
    QSpinBox,
    QVBoxLayout,
)

from fatou.gradientdialog import GradientDialog
from fatou.plugins.abstractfractal import AbstractFractalOptions


@dataclass(frozen=True)
class Preset:
    name: str


MANDELBROT_PRESETS = [
    Preset(name="Mandelbrot"),
    Preset(name="Mandelbrot3"),
    Preset(name="Mandelbrot4"),
]
MANDELBROT_PRESET_NAMES = [p.name for p in MANDELBROT_PRESETS]


class MandelbrotOptions(AbstractFractalOptions):
    typeChanged = pyqtSignal(int)
    iterationsChanged = pyqtSignal(int)
    colorDensityChanged = pyqtSignal(int, bool)
    colormapSizeChanged = pyqtSignal(int, bool)
    gradientStopsChanged = pyqtSignal(list, bool)
    juliaXChanged = pyqtSignal(float)
    juliaYChanged = pyqtSignal(float)

    smoothColoringToggle = pyqtSignal(int, bool)
    juliaToggle = pyqtSignal(int, bool)
    start = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)

        self.__readSettings()
        self._gradientDialog = None

        self.__createWidgets()
        self.__createLayout()
        self.setFixedWidth(260)

    def __readSettings(self):
        settings = QSettings("fatou", "fatou")

        settings.beginGroup("mandelbrot")
        self._current = int(settings.value("current", 0))
        self._iterationsV = int(settings.value("iterations", 50))
        self._colord = int(settings.value("color-density", 1))
        self._colors = int(settings.value("colormap-size", 512))
        self._smooth = bool(settings.value("smooth-coloring", False))
        self._juliaV = bool(settings.value("julia", False))
        self._juliaX = float(settings.value("julia-x", "0.156"))
        self._juliaY = float(settings.value("julia-y", "0.8"))
        gradient_stops = str(settings.value("gradient", ""))
        settings.endGroup()

        if not gradient_stops:
            gradient_stops = ";;".join([
                "0.00,255,255,255",
                "0.10,224,204,115",
                "0.17,198,169,6",
                "0.46,96,6,89",
                "0.72,6,128,172",
                "0.92,185,217,262",
                "1.00,255,255,255",
            ])

        gradient = []
        for value in gradient_stops.split(";;"):
            stop = value.split(",")
            alpha = float(stop[0])
            color = QColor(int(stop[1]), int(stop[2]), int(stop[3]))
            gradient.append((alpha, color))

        self._gradientStops = gradient

    def saveSettings(self):
        settings = QSettings("fatou", "fatou")

        gradient = []
        for p, color in self._gradientStops:
            r, g, b, _ = color.getRgb()
            gradient.append([p, r, g, b])
        gradient = ";;".join([",".join([str(j) for j in i]) for i in gradient])

        smooth_state = self._smoothColoring.checkState() != 0
        julia_state = self._julia.checkState() != 0

        settings.beginGroup("mandelbrot")
        settings.setValue("current", self._presets.currentIndex())
        settings.setValue("iterations", self._iterationsLimit.value())
        settings.setValue("color-density", self._colorDensity.value())
        settings.setValue("colormap-size", self._colormapSize.value())
        settings.setValue("smooth-coloring", smooth_state)
        settings.setValue("julia", julia_state)
        settings.setValue("julia-x", self._startJuliaX.value())
        settings.setValue("julia-y", self._startJuliaY.value())
        settings.setValue("gradient", gradient)
        settings.endGroup()

    def __createWidgets(self):
        self._presets = QComboBox(self)
        for f in self.presets:
            self._presets.addItem(f)
        self._presets.setCurrentIndex(self._current)

        self._startJuliaX = QDoubleSpinBox()
        self._startJuliaX.setRange(-4.0, 4.0)
        self._startJuliaX.setSingleStep(0.005)
        self._startJuliaX.setDecimals(4)
        self._startJuliaX.setMinimumWidth(80)
        self._startJuliaX.setEnabled(self._juliaV)
        self._startJuliaX.setValue(self._juliaX)

        self._startJuliaY = QDoubleSpinBox()
        self._startJuliaY.setRange(-4.0, 4.0)
        self._startJuliaY.setSingleStep(0.005)
        self._startJuliaY.setDecimals(4)
        self._startJuliaY.setMinimumWidth(80)
        self._startJuliaY.setEnabled(self._juliaV)
        self._startJuliaY.setValue(self._juliaY)

        self._iterationsLimit = QSpinBox(self)
        self._iterationsLimit.setRange(0, 1000)
        self._iterationsLimit.setValue(self._iterationsV)

        self._colorDensity = QSpinBox(self)
        self._colorDensity.setRange(0, 100)
        self._colorDensity.setMinimumWidth(80)
        self._colorDensity.setValue(self._colord)
        self._colorDensity.valueChanged.connect(self.__emitColorDensity)

        self._colormapSize = QSpinBox(self)
        self._colormapSize.setRange(2, 1024)
        self._colormapSize.setValue(self._colors)
        self._colormapSize.valueChanged.connect(self.__emitColorMapSize)

        self._smoothColoring = QCheckBox(self)
        self._smoothColoring.setChecked(self._smooth)
        self._smoothColoring.stateChanged.connect(self.__emitSmoothColoring)

        self._julia = QCheckBox(self)
        self._julia.setChecked(self._juliaV)
        self._julia.stateChanged.connect(self.__showJuliaOptions)

        self._gradientEditorButton = QPushButton("Color Editor", self)
        self._gradientEditorButton.clicked.connect(self.__showGradientEditor)

    def __createLayout(self):
        options_layout = QVBoxLayout()

        group = QGroupBox("Options")
        layout = QFormLayout()
        layout.setLabelAlignment(Qt.AlignmentFlag.AlignRight)
        layout.addRow("Presets:", self._presets)
        layout.addRow("Iterations:", self._iterationsLimit)
        layout.addRow("Color Density:", self._colorDensity)
        layout.addRow("Colormap Size:", self._colormapSize)
        layout.addRow("Smooth Coloring:", self._smoothColoring)
        layout.addRow("Switch to Julia:", self._julia)
        layout.addRow("Center X:", self._startJuliaX)
        layout.addRow("Y:", self._startJuliaY)
        group.setLayout(layout)
        options_layout.addWidget(group)
        options_layout.addWidget(
            self._gradientEditorButton, 0, Qt.AlignmentFlag.AlignHCenter
        )
        options_layout.addStretch()

        self.setLayout(options_layout)

    def __showJuliaOptions(self, state):
        state = state != 0
        self._startJuliaX.setEnabled(state)
        self._startJuliaY.setEnabled(state)
        self.juliaToggle.emit(state, True)

    def __showGradientEditor(self):
        if self._gradientDialog is None:
            self._gradientDialog = GradientDialog(self)
            self._gradientDialog.resize(400, 200)
            self._gradientDialog.show()
            self._gradientDialog.setGradientStops(self._gradientStops)
            self._gradientDialog.hide()
            self._gradientDialog.gradientChanged.connect(self.__setGradientStops)

        if self._gradientDialog.isVisible():
            self._gradientDialog.hide()
        else:
            self._gradientDialog.show()

    def __setGradientStops(self, stops):
        self._gradientStops = stops
        self.gradientStopsChanged.emit(self._gradientStops, True)

    def __emitColorDensity(self, value):
        self.colorDensityChanged.emit(value, True)

    def __emitColorMapSize(self, value):
        self.colormapSizeChanged.emit(value, True)

    def __emitSmoothColoring(self, value):
        self.smoothColoringToggle.emit(value, True)

    def emitStartData(self):
        print("emit start")
        self.emitData()
        self.start.emit()

    def emitData(self):
        self.typeChanged.emit(self._presets.currentIndex())
        self.iterationsChanged.emit(self._iterationsLimit.value())
        self.gradientStopsChanged.emit(self._gradientStops, False)
        self.colorDensityChanged.emit(self._colorDensity.value(), False)
        self.colormapSizeChanged.emit(self._colormapSize.value(), False)
        self.juliaXChanged.emit(self._startJuliaX.value())
        self.juliaYChanged.emit(self._startJuliaY.value())
        self.juliaToggle.emit(self._julia.checkState(), False)
        self.smoothColoringToggle.emit(self._smoothColoring.checkState(), False)

    def setJulia(self, x, y):
        self._startJuliaX.setValue(x)
        self._startJuliaY.setValue(y)
