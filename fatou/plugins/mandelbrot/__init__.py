# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from fatou.plugins.mandelbrot.mandelbrotexplorer import MandelbrotExplorer

__all__ = ("MandelbrotExplorer",)
