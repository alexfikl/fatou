# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtCore import pyqtSignal
from PyQt6.QtWidgets import QHBoxLayout

from fatou.plugins.abstractfractal import AbstractFractalExplorer
from fatou.plugins.mandelbrot.mandelbrot import MandelbrotWidget
from fatou.plugins.mandelbrot.mandelbrotoptions import (
    MANDELBROT_PRESET_NAMES,
    MandelbrotOptions,
)


class MandelbrotExplorer(AbstractFractalExplorer):
    display_name = "Mandelbrot and Julia Fractals"
    presets = MANDELBROT_PRESET_NAMES

    start = pyqtSignal()
    stop = pyqtSignal()
    stopped = pyqtSignal()
    saveToFile = pyqtSignal(str)
    saveSettings = pyqtSignal()
    zoomIn = pyqtSignal()
    zoomOut = pyqtSignal()
    state = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)

        self.__createWidgets()

        # from outside
        self.start.connect(self._fractalOptions.emitStartData)
        self.stop.connect(self._fractalWidget.stop)
        self.saveToFile.connect(self._fractalWidget.saveTo)
        self.zoomIn.connect(self._fractalWidget.zoomIn)
        self.zoomOut.connect(self._fractalWidget.zoomOut)
        self.saveSettings.connect(self._fractalOptions.saveSettings)

        # to outside
        self._fractalWidget.information.connect(self.state)
        self._fractalWidget.stopped.connect(self.stopped)

        # from options to widget
        self._fractalOptions.iterationsChanged.connect(
            self._fractalWidget.setIterations
        )
        self._fractalOptions.colorDensityChanged.connect(
            self._fractalWidget.setColorDensity
        )
        self._fractalOptions.colormapSizeChanged.connect(
            self._fractalWidget.setColorMapSize
        )
        self._fractalOptions.juliaXChanged.connect(self._fractalWidget.setJuliaX)
        self._fractalOptions.juliaYChanged.connect(self._fractalWidget.setJuliaY)
        self._fractalOptions.gradientStopsChanged.connect(
            self._fractalWidget.setGradientStops
        )
        self._fractalOptions.smoothColoringToggle.connect(
            self._fractalWidget.setSmoothColoring
        )
        self._fractalOptions.juliaToggle.connect(self._fractalWidget.setJulia)
        self._fractalOptions.typeChanged.connect(self._fractalWidget.setType)
        self._fractalOptions.start.connect(self._fractalWidget.start)

        # from widget to options
        self._fractalWidget.juliaCenterChanged.connect(self._fractalOptions.setJulia)

        # give all data from settings file to widget
        self._fractalOptions.emitStartData()

    def __createWidgets(self):
        self._fractalWidget = MandelbrotWidget()
        self._fractalOptions = MandelbrotOptions()

        layout = QHBoxLayout()
        layout.addWidget(self._fractalWidget)
        layout.addWidget(self._fractalOptions)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
