// SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later


#ifndef MANDELBROTRENDERER_H
#define MANDELBROTRENDERER_H

#include <QImage>
#include <QGradientStops>
#include <QDebug>

class MandelbrotRenderer: public QObject
{
    Q_OBJECT
public:
    MandelbrotRenderer();
    ~MandelbrotRenderer();

    /*
     * type = 1: render normal mandelbrot
     * type = 2: render z^3 + c
     * type = 3: render z^4 + c
     * FIXME: this should probably go in an enum. look into how that
     * interacts with PyQt. its easier like this for now.
     */
    void render(QSize imageSize, int type);

    /*
     * colors the image with the iterations from the render function
     * can be used by itself to change between smooth and normal coloring
     */
    void color(QSize size);

    void setIterations(int iterations);
    void setCenterX(double x);
    void setCenterY(double y);
    void setCenterCoordinates(double x, double y);
    void setGradientStops(QGradientStops stops);
    void setJuliaX(double x);
    void setJuliaY(double y);
    void setColorMapSize(int size);
    void setColorDensity(int density);
    void setSmoothColoring(bool coloring);
    void setJulia(bool julia);
    void setScale(double scale);

    void abort();

    double centerX();
    double centerY();
    double scale();
    bool julia();
signals:
    void completed(const QImage &image, double scaleFactor);

private:
    double smoothIterations(int its, double zx, double zy);
    uint getColorFor(int i);

    int iterate(double x, double y, int type);
    int mandelbrot2(double zx, double zy, double cx, double cy); /* z^2 + c */
    int mandelbrot3(double zx, double zy, double cx, double cy); /* z^3 + c */
    int mandelbrot4(double zx, double zy, double cx, double cy); /* z^4 + c */

    double m_centerX;
    double m_centerY;
    double m_juliaX;
    double m_juliaY;

    bool m_julia;
    bool m_smoothColoring;
    bool m_abort;

    double m_scaleFactor;
    uint m_iterations;

    QList<int> m_iterationsList;
    QList<double> m_realIterationsList;
    QList<uint> m_colormap;

    uint m_colorMapSize;
    uint m_colorDensity;
};

#endif
