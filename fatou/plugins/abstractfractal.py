# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtWidgets import QWidget


class AbstractFractalOptions(QWidget):
    """Abstract options panel."""

    def __init__(self, parent=None):
        super().__init__(parent)


class AbstractFractalExplorer(QWidget):
    """Abstract Plugin For Fractal Objects.

    Recommended functions:
    - ``writeSettings()``: for saving options. If it is defined, it is
      automatically called at application closing and when selecting a different
      plugin.
    - ``saveToFile(filename)``: saves the fractal as an image.
    - ``start()``: start rendering the fractal with given options.
    - ``stop()``: stop rendering.

    Recommended signals:
    - ``state()``: let the application know the current state of the plugin.
    """

    def __init__(self, parent=None):
        super().__init__(parent)
