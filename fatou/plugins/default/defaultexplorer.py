# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QFont, QPainter

from fatou.plugins.abstractfractal import AbstractFractalExplorer


class DefaultExplorer(AbstractFractalExplorer):
    presets = ()
    display_name = "Default"

    def __init__(self, parent=None):
        super().__init__(parent)

    def paintEvent(self, ev):
        painter = QPainter(self)
        font = QFont("DejaVu", 20)

        painter.fillRect(self.rect(), Qt.GlobalColor.black)
        painter.setPen(Qt.GlobalColor.white)
        painter.setFont(font)
        painter.drawText(self.rect(), Qt.AlignmentFlag.AlignCenter, "Welcome to Fatou!")
