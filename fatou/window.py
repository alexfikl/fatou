# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtCore import QDir, QPoint, QSettings, QSize, Qt, pyqtSignal
from PyQt6.QtWidgets import (
    QApplication,
    QFileDialog,
    QLabel,
    QMainWindow,
    QSizePolicy,
    QWidget,
)

from fatou.fractalselection import FractalSelectionDialog
from fatou.plugins.default.defaultexplorer import DefaultExplorer


class FatouWindow(QMainWindow):
    saveToFile = pyqtSignal(str)

    def __init__(self, am, pm):
        super().__init__()

        self._readSettings()

        # init stuff
        self._pluginManager = pm
        # TODO: better name for these things. explorer doesn't sound very descriptive
        self._explorer = DefaultExplorer(self)
        self._selectDialog = None
        self._pluginState = "No plugins loaded."
        self._actionManager = am
        self._actions = {
            "save": None,
            "start": None,
            "stop": None,
            "select": None,
            "zoom-in": None,
            "zoom-out": None,
        }

        # connect slots
        QApplication.instance().aboutToQuit.connect(self.saveSettings)

        # set up interface
        self._createActions()
        self._createMenus()
        self._createToolbars()
        self._createStatusBar()

        # done!
        self.setCentralWidget(self._explorer)

    def showSelectDialog(self):
        if self._selectDialog is None:
            self._selectDialog = FractalSelectionDialog(self)
            self._selectDialog.setPluginList(self._pluginManager.listPlugins(), 0)
            self._selectDialog.loadPlugin.connect(self.setMainWidget)
            self._pluginManager.pluginListChanged.connect(
                self._selectDialog.setPluginList
            )

        self._selectDialog.setVisible(not self._selectDialog.isVisible())

    def setMainWidget(self, name):
        # NOTE: do all the things that were connected to it automatically
        # disconnect?
        # get new plugin
        plugin = self._pluginManager.getPlugin(name)
        self._explorer = plugin.entrypoint(self)

        # set properties
        self.setWindowTitle(f"Fatou - {plugin.entrypoint.display_name}")
        self.setCentralWidget(self._explorer)

        # check supported features
        self._checkPluginFeatures()

    def saveSettings(self):
        # TODO: maybe save last used plugin and other stuff?
        settings = QSettings("fatou", "fatou")
        settings.beginGroup("main")
        settings.setValue("size", self.size())
        settings.setValue("position", self.pos())
        settings.endGroup()

    def _readSettings(self):
        settings = QSettings("fatou", "fatou")

        settings.beginGroup("main")
        self.resize(settings.value("size", QSize(950, 600)))
        self.move(settings.value("position", QPoint(100, 100)))
        settings.endGroup()

    def _createStatusBar(self):
        self._fractalState = QLabel("No Info")
        self.statusBar().addPermanentWidget(self._fractalState)
        self.statusBar().showMessage("Ready")

    def _createActions(self):
        # TODO: alternative: look into doing it in a '.ui' file
        self._actions["save"] = self._actionManager.getStandardAction("save")
        self._actions["save"].setEnabled(False)

        self._actions["stop"] = self._actionManager.getStandardAction("stop")
        self._actions["stop"].setEnabled(False)

        self._actions["start"] = self._actionManager.getStandardAction("start")
        self._actions["start"].setEnabled(False)

        self._actions["zoom-in"] = self._actionManager.getStandardAction("zoom-in")
        self._actions["zoom-in"].setEnabled(False)

        self._actions["zoom-out"] = self._actionManager.getStandardAction("zoom-out")
        self._actions["zoom-out"].setEnabled(False)

        self._actions["select"] = self._actionManager.getStandardAction("select")
        self._actions["select"].triggered.connect(self.showSelectDialog)

    def _checkPluginFeatures(self):
        if hasattr(self._explorer, "start"):
            self._actions["start"].setEnabled(True)
            self._actions["start"].triggered.connect(self._explorer.start)
            self._actions["start"].triggered.connect(self._disableStart)
            self._actions["start"].triggered.connect(self._statusStarted)
        else:
            self._actions["start"].setEnabled(False)

        if hasattr(self._explorer, "stop"):
            self._actions["stop"].setEnabled(True)
            self._actions["stop"].triggered.connect(self._explorer.stop)
            self._actions["stop"].triggered.connect(self._enableStart)
        else:
            self._actions["stop"].setEnabled(False)

        if hasattr(self._explorer, "zoomIn"):
            self._actions["zoom-in"].setEnabled(True)
            self._actions["zoom-in"].triggered.connect(self._explorer.zoomIn)
        else:
            self._actions["zoom-in"].setEnabled(False)

        if hasattr(self._explorer, "zoomOut"):
            self._actions["zoom-out"].setEnabled(True)
            self._actions["zoom-out"].triggered.connect(self._explorer.zoomOut)
        else:
            self._actions["zoom-out"].setEnabled(False)

        if hasattr(self._explorer, "saveToFile"):
            self._actions["save"].setEnabled(True)
            self._actions["save"].triggered.connect(self._getFilename)
            self.saveToFile.connect(self._explorer.saveToFile)
        else:
            self._actions["save"].setEnabled(False)

        if hasattr(self._explorer, "stopped"):
            self._explorer.stopped.connect(self._statusStopped)

        if hasattr(self._explorer, "state"):
            self._explorer.state.connect(self._setState)

        if hasattr(self._explorer, "saveSettings"):
            # save plugin settings when we exit
            QApplication.instance().aboutToQuit.connect(self._explorer.saveSettings)
            # save plugin settings when we change to another one
            self._selectDialog.loadPlugin.connect(self._explorer.saveSettings)

    def _createMenus(self):
        # TODO: add more of the actions here. way too slim
        computation_menu = self.menuBar().addMenu("Calculate")
        computation_menu.addAction(self._actions["save"])

    def _createToolbars(self):
        spacer = QWidget()
        spacer.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)

        toolbar = self.addToolBar("Main")
        toolbar.setToolButtonStyle(Qt.ToolButtonStyle.ToolButtonTextUnderIcon)
        toolbar.addAction(self._actions["save"])
        toolbar.addAction(self._actions["start"])
        toolbar.addAction(self._actions["stop"])
        toolbar.addSeparator()
        toolbar.addAction(self._actions["zoom-in"])
        toolbar.addAction(self._actions["zoom-out"])
        toolbar.addWidget(spacer)
        toolbar.addAction(self._actions["select"])

    def _getFilename(self):
        filename = QFileDialog.getOpenFileName(
            self, "Save Fractal", QDir.homePath(), "PNG Image (*.png)"
        )

        # NOTE: should this be isEmpty()?
        if not filename.isNull():
            if not filename.endsWith(".png", Qt.CaseSensitivity.CaseInsensitive):
                filename += ".png"
            self.saveToFile.emit(filename)

    def _setState(self, state):
        self._pluginState = str(state)

    def _disableStart(self):
        # when plugin is working we can't start it again
        # NOTE: merge this with _statusStarted? why are they 'private'?
        self._actions["save"].setEnabled(False)

    def _statusStarted(self):
        self.statusBar().showMessage("Computing...")

    def _enableStart(self):
        self._actions["save"].setEnabled(True)
        self.statusBar().showMessage("Ready")

    def _statusStopped(self):
        self.statusBar().showMessage("Finished", 2000)
        self._actions["save"].setEnabled(True)
