# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from PyQt6.QtCore import Qt, pyqtSignal
from PyQt6.QtWidgets import QDialog, QHBoxLayout, QPushButton, QVBoxLayout

from fatou.gradients.gradienteditor import GradientEditor


class GradientDialog(QDialog):
    """Dialog presenting the color picker."""

    gradientChanged = pyqtSignal(list)

    def __init__(self, parent=None):
        super().__init__(parent)

        self._gradientEditor = GradientEditor(self)

        close = QPushButton("Close")
        close.clicked.connect(self.hide)

        layout = QVBoxLayout()
        layout.addWidget(self._gradientEditor)
        box_layout = QHBoxLayout()
        box_layout.addWidget(close, 0, Qt.AlignmentFlag.AlignRight)
        layout.addLayout(box_layout)
        self.setLayout(layout)

        self._gradientEditor.gradientStopsChanged.connect(self.gradientChanged)

    def setGradientStops(self, stops):
        """Sets the QGradientStops in the GradientEditor"""
        self._gradientEditor.setGradientStops(stops)
