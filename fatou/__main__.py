#!/usr/bin/env python

# SPDX-FileCopyrightText: 2011-2023 Alexandru Fikl <alexfikl@gmail.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import sys

import fatou
from fatou.actionmanager import ActionManager
from fatou.pluginmanager import PluginManager
from fatou.window import FatouWindow


def main():
    from PyQt6.QtWidgets import QApplication

    app = QApplication(sys.argv)
    app.setApplicationName(fatou.__name__)
    app.setApplicationVersion(fatou.__version__)

    pm = PluginManager()
    am = ActionManager()
    window = FatouWindow(am, pm)
    window.setWindowTitle("Fatou")
    window.show()

    sys.exit(app.exec())


if __name__ == "__main__":
    main()
