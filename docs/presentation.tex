\documentclass{beamer}
\usepackage[utf8x]{inputenc}
\usetheme{Dresden}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}

\usepackage{listings}
\usepackage{alltt}
\usepackage{algorithmic}

\usepackage{graphicx}
\usepackage{float}

\title{Fractal Visualizer}
\author{Alexandru Fikl}
\institute{West University of Timişoara \\Faculty of Mathematics and Informatics}
\date{04/07/2011\\Diploma Thesis Presentation}

\definecolor{darkgray}{RGB}{69, 69, 69}
\setbeamercolor{normal text}{fg=darkgray}

\begin{document}

\begin{frame}
 \titlepage
\end{frame}

\begin{frame}
 \frametitle{Table of Contents}
 \tableofcontents
\end{frame}

\section{Introduction}
\begin{frame}{Overview}
\begin{itemize}[<+-| alert@+>]
    \item Fractal objects have shown that determinism and high complexity are not
    mutually exclusive.
    \item Fractal geometry is used to model nature.
    \item All fractals objects are based on iteration, making them part of the study
    of dynamical systems.
    \item A dynamical system is basically composed of a state and a function or
    rule. Anything that evolves over time can be thought of as a dynamical system.
\end{itemize}
\end{frame}

\begin{frame}{What Are Fractals?}

\begin{itemize}[<+-| alert@+>]
    \item Beno\^{i}t Mandelbrot coined the term in 1975, said to come from the latin
    \emph{fractus} meaning \emph{broken} or \emph{fragmented}.
    \item Mandelbrot's definition: a set for which the Hausdorff
dimension strictly exceeds the topological dimension.
    \item A fractal is a rough or fragmented geometric shape that can be
subdivided in parts, each of which is a reduced-size copy of the whole,
i.e. exhibits self-similarity.
    \item Fractals are used in: landscape generation, music generation, art
generation, signal and image compression, cosmology, seismology, video games etc.
\end{itemize}
\end{frame}

\section{Fractals}
\begin{frame}{Mandelbrot Fractal - Theory}
\begin{itemize}[<+-| alert@+>]
    \item Given the function $f_c(z) = z^2 + c$
    \item The Mandelbrot Set is made up of all the points $c$ such that the orbit
of the defined function is not infinite.
    \item The orbit is defined as:
\[O(z) = \{0, f_c(0), f_c(f_c(0)), \dots \}\]
    \item Can be generalized with $f_c(z) = z^n + c$.
    \item Buddhabrot: a recent discovery. Basically just a different way of displaying
the Mandelbrot Set.
    \item Relation between the Julia Set and the Mandelbrot Set
$M = \{c \in \mathbb{C} | J_{f_c} \text{ is connected}\}$.
\end{itemize}
\end{frame}

\begin{frame}{Mandelbrot Fractal - Pictures}
\begin{center}
\includegraphics[scale=0.83]{./images/julia_mandel/mandelbrot-julia.png}
\end{center}
\end{frame}

\begin{frame}{Mandelbrot Fractal - Pictures II}
\begin{columns}[c]
\column{1.5in}
\framebox{\includegraphics[scale=0.20]{./images/julia_mandel/mandel4.png}} \\
\centering{$f_c(z) = z^4 + c$}

\column{1.5in}
\includegraphics[scale=0.22]{./images/julia_mandel/buddhabrot.png} \\
\centering{Buddhabrot}
\end{columns}
\end{frame}

\begin{frame}{Julia Fractal - Theory}
\footnotesize
\begin{itemize}[<+-| alert@+>]
    \item Given a function $R: \hat{\mathbb{C}} \rightarrow \hat{\mathbb{C}}$,
$R(Z) = \frac{P(Z)}{Q(Z)}$.
    \item We are only considering periodic orbits, if the orbit is not periodic,
then the point does not belong to the Julia Set.
    \item A periodic point can be either repelling or attractive. Only repelling
points are part of the Julia Set. The points that are not in the Julia Set are
in the Fatou Set.
    \item Most images are of the filled Julia Set, the actual Julia Set is the
boundary of the filled one.
\end{itemize}
\end{frame}

\begin{frame}{Julia Fractal - Algorithm}
Known as the `Escape Time Algorithm'.
\begin{algorithmic}[<+-| alert@+>]
    \STATE c = fixed \COMMENT{the Julia Set constant c}
    \FORALL {pixels on the screen}
        \STATE $z = a + b*i$ \COMMENT{a, b are scaled coordinates of the pixels}
        \WHILE {z has not escaped AND maximum iterations not reached}
            \STATE $z \mapsto z^2 + c$
        \ENDWHILE
        \IF {maximum iterations not reached}
            \STATE $J_{R_c} = J_{R_c} \cup \{z\}$
        \ENDIF
    \ENDFOR
\end{algorithmic}
\end{frame}

\begin{frame}{Julia Fractal - Pictures}
Images generated with the function $z^2 + c$.
\begin{columns}[c]
\column{1.5in}
\framebox{\includegraphics[scale=0.25]{./images/julia_mandel/julia--061.png}}\\
\centering{$c = -0.825 - 0.232i$}

\column{1.5in}
\includegraphics[scale=0.44]{./images/julia_mandel/julia-color.png}\\
\centering{$c = (\varphi - 2) + (\varphi - 1)i$}
\end{columns}
\end{frame}

\begin{frame}{Newton Fractal - Theory}
\footnotesize
\begin{itemize}[<+-| alert@+>]
    \item Based on the Newton-Raphson Method for finding the roots of a function
    \item We iterate from a chosen starting point $x_0$
\[
    x_{n + 1} = x_n - \frac{f(x_n)}{f'(x_n)}, \quad n = 1, 2, \dots.
\]
    \item Usually each iteration gets closer to a root of a function.
\begin{center}
\includegraphics[scale=0.4]{./images/newton/graph.png}
\end{center}
    \item The method may not converge if our starting point is not well chosen.
\end{itemize}
\end{frame}

\begin{frame}{Newton Fractal - Theory II}
\begin{itemize}[<+-| alert@+>]
    \item The roots $\xi$ of f each have a basin of attraction:
\[ A_n = \{ x \in \mathbb{C} | \lim_{n \to \infty} x_n = \xi \}. \]
    \item The boundaries of these basins form the Julia Set of the function.
    \item As a dynamical system:
\[N_f: \mathbb{C} \rightarrow \mathbb{C}, \quad N_f(x) = x - \frac{f(x)}{f'(x)}.\]
    \item Newton Fractals can even imitate a Julia fractal.
\end{itemize}
\end{frame}

\begin{frame}{Newton Fractal - Imitation}
\tiny
\begin{columns}[c]
\column{1.5in}
\includegraphics[scale=0.35]{./images/julia_mandel/julia-newton.png} \\
\centering{$c = 0.28 + 0.528i$}

\column{3in}
\includegraphics[scale=0.70]{./images/newton/newton-julia.png} \\
$f(z) = (z - (0.9994 - 0.5286i))^{(-0.4722-0.4998i)}(z - (0.0006+0.5286i))^{(0.4722+0.4998i)}$
\end{columns}
\end{frame}

\begin{frame}{Newton Fractal - Algorithm}
\small
\begin{algorithmic}
    \STATE Take a function $f(x)$
    \FORALL {pixels on the screen}
        \STATE $x_0 = x + yi$ \COMMENT{x, y are scaled coordinates of the pixels}
        \WHILE {x has not reached a fixed point AND maximum iterations not reached}
            \STATE $x_{k+1} = x_k - \frac{f(x_k)}{f'(x_k)}$ \COMMENT{apply the
Newton-Raphson Method}
        \ENDWHILE
        \STATE Color pixel according to the root reached.
    \ENDFOR
\end{algorithmic}
\end{frame}

\begin{frame}{Newton Fractal - Pictures}
\begin{columns}[c]
\column{1.5in}
\includegraphics[scale=0.40]{./images/newton/newton-roots2.png} \\
\centering{$f(x) = x^3 - 2x + 2$}

\column{1.5in}
\includegraphics[scale=0.40]{./images/newton/newton-roots.png} \\
\centering{$f(x) = x^4 - 1$}
\end{columns}
\end{frame}

\begin{frame}{Iterated Function Systems - Theory}
\footnotesize
\begin{itemize}[<+-| alert@+>]
    \item An Iterated Function System consists of a metric space $(X, d)$ and a finite
set of contraction mappings $f_i: X \to X$.
    \item A contraction mapping is a transformation $f: X \to X$ on the metric space
that has the following property:
\[d(f(x), f(y)) \leq sd(x, y), \quad \forall x, y \in X, \quad s \in (0,1]\]
    \item Two dimensional affine transformations:
\[w(x) = w
\begin{pmatrix}
x_1 \\
x_2
\end{pmatrix}
=
\begin{pmatrix}
a & b \\
c & d
\end{pmatrix}
\begin{pmatrix}
x_1 \\
x_2
\end{pmatrix}
+
\begin{pmatrix}
e \\
f
\end{pmatrix}\]
    \item Given an IFS, after infinitely many transformations we obtain its
attractor set.
    \item The Collage Theorem is the inverse operation. Given an attractor set
we have to deduce the transformations.
\end{itemize}
\end{frame}

\begin{frame}{Iterated Function Systems - Pictures}
\begin{columns}[c]
\column{1.5in}
\includegraphics[scale=0.10]{./images/ifs/triangle-1000.png} \\
\centering{1.000 iterations}

\column{1.5in}
\includegraphics[scale=0.10]{./images/ifs/triangle-10000.png} \\
\centering{10.000 iterations}
\end{columns}

\begin{columns}[c]
\column{1.5in}
\includegraphics[scale=0.10]{./images/ifs/triangle-100000.png} \\
\centering{100.000 iterations}

\column{1.5in}
\tiny\[w_1
\begin{pmatrix}
x \\
y
\end{pmatrix}
=
\begin{pmatrix}
0.5 & 0 \\
0.5 & 0
\end{pmatrix}
\begin{pmatrix}
x \\
y
\end{pmatrix}
+
\begin{pmatrix}
0 \\
0
\end{pmatrix}
\]
\[w_2
\begin{pmatrix}
x \\
y
\end{pmatrix}
=
\begin{pmatrix}
0.5 & 0.0 \\
0.5 & 0.0
\end{pmatrix}
\begin{pmatrix}
x \\
y
\end{pmatrix}
+
\begin{pmatrix}
0.0 \\
1.0
\end{pmatrix}
\]
\[w_3
\begin{pmatrix}
x \\
y
\end{pmatrix}
=
\begin{pmatrix}
0.5 & 0.0 \\
0.5 & 1.0
\end{pmatrix}
\begin{pmatrix}
x \\
y
\end{pmatrix}
+
\begin{pmatrix}
0.0 \\
1.0
\end{pmatrix}
\]
\end{columns}
\end{frame}

\begin{frame}{Lindenmayer Systems - Theory}
\begin{itemize}[<+-| alert@+>]
    \item Named after Aristid Lindenmayer who developed them to model algae.
    \item Based on string rewriting systems (semi-Thue Systems).
    \item Applies rules in parallel, not sequentially like a string rewriting
system.
    \item The parallelism is due to the biological motivation: cell divisions in
an organism happen in parallel.
    \item The simplest type of L-systems is known as D0L-systems (deterministic
and context-free).
\end{itemize}
\end{frame}

\begin{frame}{Lindenmayer Systems - Theory II}
\begin{itemize}[<+-| alert@+>]
    \item A L-system is an ordered triplet $G = \langle \Sigma, w, P \rangle$ where
    \begin{itemize}
        \item $\Sigma$ is the alphabet of the system (contains all the symbols)
        \item $w \in \Sigma^+$ is a non-empty word known as the axiom
        \item $P \subset \Sigma \times \Sigma^*$ is a set of rewriting rules.
    \end{itemize}
    \item L-systems are visualized using the turtle interpretation of strings.
\end{itemize}
\end{frame}

\begin{frame}{Lindenmayer Systems - Algorithm}
\begin{algorithmic}
    \STATE Given:
    \begin{itemize}
        \item an alphabet $\Sigma$ and the turtle interpretation of each symbol
        \item an axiom $w$
        \item a set of rewrite rules $R$
        \item the number of iterations $n$
    \end{itemize}

    \FOR{$i = 1 \to n \text{ iterations}$}
        \FORALL{symbols in the axiom}
            \STATE apply the rewrite rule that has the symbol in the left-hand side
        \ENDFOR
    \ENDFOR

    \FORALL{symbols in the final string}
        \STATE apply the turtle interpretation of the symbol
    \ENDFOR
\end{algorithmic}
\end{frame}

\begin{frame}{Lindenmayer Systems - Example}
Common meanings of symbols used in turtle graphics:

\begin{center}
\begin{tabular}{ll}
\textbf{Symbol} & \textbf{Meaning} \\
F, A, B & Move forward a step of size d. The state of the turtle changes to \\
& $(x', y', \alpha)$, where $x' = x + d \cos \alpha$ and $y' = y + d \sin \alpha$. \\
& Also a line is drawn from the point $(x, y)$ to the point $(x', y')$. \\
f & Move forward without drawing a line. \\
+ & Turn Left by angle $\delta$. The state of the turtle changes \\
& to $(x, y, \alpha + \delta)$. \\
- & Turn Right by angle $\delta$, The state changes to $(x, y, \alpha - \delta)$. \\
$[$ & Store the current state of the turtle. \\
$]$ & Restore the last stored state of the turtle.
\end{tabular}
\end{center}
\end{frame}

\begin{frame}{Lindenmayer Systems - Example II}
\footnotesize
\begin{columns}[c]
\column{2in}
\begin{tabular}{ll}
\textbf{Alphabet} & A, B \\
\textbf{Constants} & none \\
\textbf{Axiom} & A \\
\textbf{Rewrite Rules} & A $\rightarrow$ B \\
& A $\rightarrow$ AB
\end{tabular}

\column{3in}
n = 0 : A

n = 1 : B

n = 2 : AB

n = 3 : BAB

n = 4 : ABBAB

n = 5 : BABABBAB

n = 6 : ABBABBABABBAB

n = 7 : BABABBABABBABBABABBAB
\end{columns}
\end{frame}

\begin{frame}{Lindenmayer Systems - Pictures}
\tiny
\begin{columns}[c]
\column{1.5in}
\includegraphics[scale=0.45]{./images/lsystems/plant3.png} \\
\hbox{}
\begin{tabular}{ll}
\textbf{Alphabet} & X, F \\
\textbf{Constants} & +, -, [, ] \\
\textbf{Axiom} & X \\
\textbf{Angle} & $25.7^{\circ}$ \\
\textbf{Rewrite Rules} & X $\rightarrow$ F[+X][-X]FX \\
& F $\rightarrow$ FF
\end{tabular}

\column{1.5in}
\includegraphics[scale=0.30]{./images/lsystems/islands.png} \\
\hbox{}
\begin{tabular}{ll}
\textbf{Alphabet} & F, f \\
\textbf{Constants} & +, - \\
\textbf{Axiom} & F+F+F+F \\
\textbf{Angle} & $90^{\circ}$ \\
\textbf{Rewrite Rules} & F $\rightarrow$ F+f-FF+F+FF+Ff+FF- \\
& f+FF-F-FF-Ff-FFF \\
& f $\rightarrow$ ffffff
\end{tabular}
\end{columns}

\end{frame}

\begin{frame}{Cellular Automata - Theory}
\begin{itemize}[<+-| alert@+>]
    \item First developed by John von Neumann and Stanislaw Ulam.
    \item A cellular automaton $A$ is a system $(l^n, g, Q, \sigma)$ where:
    \begin{itemize}
        \item $l^n$ is the underlying space and $\alpha = (a_1, a_2, \dots , a_n)$
is a cell.
        \item $g: l^n \to (l^n)^m$ is the neighborhood function defined by
$g(\alpha) = (\alpha + \delta_1, \alpha + \delta_2, \dots, \alpha + \delta_m)$
where $\delta_i \in l^n$ is fixed.
        \item $Q$ is a finite set of cell states.
        \item $\sigma : Q^m \to Q$ is a local transition function.
\end{itemize}
    \item Now famous through John Conway's Game of Life and the work of Stephen Wolfram
on Elementary Cellular Automata.
\end{itemize}
\end{frame}

\begin{frame}{Cellular Automata - Theory II}
\footnotesize
\begin{itemize}[<+-| alert@+>]
    \item The Game of Life takes place on a two dimensional grid of cells which
have only two states: DEAD or ALIVE.
    \item The rules that govern the game are:
    \begin{itemize}
        \item any live cell with less than two alive neighbors or with more than
four dies.
        \item any dead cell with exactly 3 live neighbors becomes alive.
        \item any live cell with two or three live neighbors does not change state.
    \end{itemize}
    \item The Game of Life exhibits Turing Completeness.
    \item The Elementary Automata take place on a unidimensional array and their
rules are based on the current cell and its two neighbors. There is a total of 256
possible rules.
    \item Only Rule 110 exhibits universal computation like the Turing Machine.
\end{itemize}
\end{frame}

\begin{frame}{Cellular Automata - Pictures}
\begin{columns}[c]
\column{1.5in}
\includegraphics{./images/game-of-life/pulsar.png} \\
\centering{A famous oscillator}

\column{1.5in}
\includegraphics[scale=0.55]{./images/elementary/rule-110.png} \\
\centering{Rule 110}
\end{columns}
\end{frame}

\section{Application}

\begin{frame}{Application - Programming}
\begin{itemize}
    \item Written in Python and some C++.
    \item Using the Qt C++ framework.
    \item Bindings provided by PyQt.
    \item[]
\end{itemize}
\end{frame}

\begin{frame}{Application - Software Design}
The class diagram:
\begin{center}
\includegraphics[scale=0.4]{./images/class-diagram.png}
\end{center}
\end{frame}

\begin{frame}{Application - Plugins}
\begin{itemize}[<+-| alert@+>]
    \item The plugin folder structure must be as follows:
\textbf{\begin{itemize}
    \item[=] pluginname/
    \begin{itemize}
        \item[/] pluginnameexplorer.py
        \item[/] other files.
    \end{itemize}
\end{itemize}}
    \item In the pluginnameexplorer.py file there has to be a class with the
name PluginnameExplorer, with only the first letter uppercased.
    \item If these requirements are not met the plugin shall be rejected by the
PluginManager. Plugins are installed in the form of a zip file.
\end{itemize}
\end{frame}

\begin{frame}{Application - Plugins II}
Available plugins are:
\begin{itemize}[<+-| alert@+>]
    \item A Mandelbrot and Julia plugin.
    \item A Newton plugin that can support user submitted formulas.
    \item An Iterated Function Systems plugin that has several default mappings,
but also supports user submitted ones.
    \item A Lindenmayer Systems plugin that already implements most famous systems,
but lets the user choose his own rewrite rules.
    \item A Cellular Automata plugin that supports the Game of Life and
Elementary Cellular Automata.
\end{itemize}
\end{frame}

\begin{frame}{Application - Future Work}
\begin{itemize}
    \item Overall optimization, solid guessing.
    \item Mandelbrot: user submitted formulas, Buddhabrot.
    \item Newton: more coloring options, more robust equation solver.
    \item Cellular Automata: variations on the Game of Life, optimizations.
    \item Iterated Function Systems: fractal image compression.
    \item Add strange attractors, landscape generation, computation of fractal
dimension and edge detection, filigrams and others.
\end{itemize}

\end{frame}

\section{Questions}
\begin{frame}
\begin{center}
\LARGE
Thank You for Your Attention!

\hbox{}

QUESTIONS?
\end{center}
\end{frame}

\end{document}
